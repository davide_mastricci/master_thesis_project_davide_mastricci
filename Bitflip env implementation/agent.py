import numpy as np
import random
from model import DQN
from environment import Env
import torch
import os
import matplotlib.pyplot as plt
import math
import time


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class Agent():
    # Experience replay buffer  https://github.com/localminimum/hindsight-experience-replay/blob/master/HER.py
    class Buffer():
        def __init__(self, buffer_size=50000):
            """
            This is an inner class implementing the replay buffer.
            Credits for this implementation to: https://github.com/localminimum/hindsight-experience-replay/blob/master/HER.py

            Args:
                buffer_size (int): It's the size of the buffer indicating how much knowledge of the past episodes the
                agent has to keep. The default value is 50000.
            """
            self.buffer = []
            self.buffer_size = buffer_size

        def add(self, experience):
            """
            This method add a transaction inside the buffer. If the buffer is full then the new transaction will
            overwrite the oldest one.

            Args:
                experience (numpy.array): the transaction must be a vector like (state, action, reward, next_state)
                with the shape [1, 4].
            """
            self.buffer.append(experience)
            if len(self.buffer) > self.buffer_size:
                self.buffer = self.buffer[int(0.0001 * self.buffer_size):]

        def sample(self, size):
            """
            This method sample a random batch of transactions of dimensions ['size', 4].

            Args:
                size (int): the number of transaction to be sampled. It must be a positive integer.

            Returns:
                batch (numpy.array): a matrix of dimensions ['size, 4] containing the transactions sampled.
            """
            if len(self.buffer) >= size:
                experience_buffer = self.buffer
            else:
                experience_buffer = self.buffer * size
            return np.copy(np.reshape(np.array(random.sample(experience_buffer, size)), [size, 4]))

    def __init__(self, args):
        """
        This class implements a DQN agent which can solve the bitflipping task using Hindsight Experience Replay
        techniques.

        Args:
             args (argparse.ArgumentParser): a list of parameters which control the learning process of the agent
             and where save the results. That list can be found inside the arguments.py file.
        """

        self.args = args
        np.random.seed(self.args.seed)
        # build up the network
        self.q_net = DQN(self.args.num_inputs * 2, self.args.num_inputs)
        # get the target
        self.q_target_net = DQN(self.args.num_inputs * 2, self.args.num_inputs)

        if self.args.cuda:
            self.q_net.cuda()
            self.q_target_net.cuda()

        # copy parameters..
        self.q_target_net.load_state_dict(self.q_net.state_dict())
        self.q_target_net, self.q_net = self.updateTarget(self.q_target_net, self.q_net)
        # set up the optimizer
        self.optimizer = torch.optim.Adam(self.q_net.parameters(), lr=self.args.net_lr)

        # self.q_net.train()
        # self.q_target_net.eval()

        # create direcories to save results
        if not os.path.exists(self.args.save_dir):
            os.mkdir(self.args.save_dir)
        self.model_path = self.args.save_dir + self.args.env_name + '/'
        if not os.path.exists(self.model_path):
            os.mkdir(self.model_path)

    def updateTarget(self, target, source):
        """
        This method allows the transfer of the weights from the source network to the target.
        The update is done using a 'tau' parameter specified in the arguments.py file.
        Target and source must be two objects of the some class.

        Args:
            target (torch.nn.Module): It is the target network.
            source (torch.nn.Module): It is the source network.

        Returns:
            target (torch.nn.Module): The updated target network.
            source (torch.nn.Module): The original source network.
        """
        # update the critic network firstly...
        for target_param, param in zip(target.parameters(), source.parameters()):
            target_param.data.copy_(self.args.tau * param.data + (1 - self.args.tau) * target_param.data)
        return target, source

    def train(self):
        """
        This method contains the core procedure to train the agent.
        The methodology used to train the agent is the same as the one described into Hindsight Experience Replay paper.
        The strategy for sampling virtual goals is the k-future, where k is specified inside arguments.py.
        During the training this method shows a real-time plot of the loss function of Q-network and the success rate,
        at the end the final plot and the model are saved in ./[save_dir]/Bitflip/, where [save_dir] is specified inside
        arguments.py.

        Note:
            Based on the parameters in arguments.py this method will perform the agent training using one of the
            following configurations:
            1) Sparse reward function with HER replay
            2) Sparse reward function without HER replay
            3) Shaped reward function without HER replay
            4) Shaped reward function with HER replay

            So far this method is tested to work with the 1st configuration.
        """

        # setting up buffer
        replay_buffer = self.Buffer(self.args.replay_size)
        env = Env(self.args.num_inputs)
        succeed = 0
        success_rate = []
        total_loss = []
        total_rewards = []

        # set up plot
        plt.ion()
        fig = plt.figure()
        ax = fig.add_subplot(211)
        plt.title("Success Rate")
        ax.set_ylim([0, 1.])
        ax2 = fig.add_subplot(212)
        plt.title("Q Loss")
        line = ax.plot(np.zeros(1), np.zeros(1), 'r-')[0]
        line2 = ax2.plot(np.zeros(1), np.zeros(1), 'r-')[0]
        fig.canvas.draw()

        # start experiencing
        for epoch_i in range(self.args.max_epochs):
            for cycle_i in range(self.args.max_cycles):
                total_reward = 0.0
                successes = []
                for episode_i in range(self.args.max_episode):
                    env.reset()
                    episode_experience = []
                    episode_succeeded = False
                    for t in range(self.args.num_inputs):
                        state = np.copy(env.state)
                        goal = np.copy(env.target)
                        inputs = np.concatenate([state, goal], axis=-1)
                        inputs_tensor = torch.tensor(inputs.tolist(), dtype=torch.float32).unsqueeze(0)
                        if self.args.cuda:
                            inputs_tensor = inputs_tensor.cuda()
                        with torch.no_grad():
                            action, _ = self.q_net(inputs_tensor)

                        # select the actions
                        action = self._select_actions(action, cycle_i)
                        action = action[0]
                        # print(action)
                        # step
                        # print('action\t'+str(action))
                        # print("GAOL\t"+str(goal))
                        state_, reward = env.step(action)
                        # print("ACHIEVED\t"+str(state_))
                        episode_experience.append((state, action, reward, state_, goal))
                        # print(reward)
                        total_reward += reward
                        # print(reward)
                        if reward == 0:
                            if episode_succeeded:
                                continue
                            else:
                                episode_succeeded = True
                                succeed += 1
                    successes.append(episode_succeeded)
                    for t in range(self.args.num_inputs):
                        state, action, reward, state_, goal = episode_experience[t]
                        inputs = np.concatenate([state, goal], axis=-1)
                        next_inputs = np.concatenate([state_, goal], axis=-1)
                        replay_buffer.add(np.reshape(np.array([inputs, action, reward, next_inputs]), [1, 4]))
                        if self.args.HER:
                            for k in range(self.args.k_future):
                                future = np.random.randint(t, self.args.num_inputs)
                                _, _, _, virtual_goal, _ = episode_experience[future]
                                inputs = np.concatenate([state, virtual_goal], axis=-1)
                                next_inputs = np.concatenate([state_, virtual_goal], axis=-1)
                                check_reward = np.sum(state_ == virtual_goal) == self.args.num_inputs
                                # print(check_reward)
                                # credits: https://github.com/localminimum/hindsight-experience-replay/blob/master/HER.py
                                if self.args.shaped_function:
                                    # print("Shaped")
                                    reward = 0 if check_reward else -np.sum(np.square(np.array(state_) == \
                                                                                      np.array(virtual_goal)))
                                else:
                                    # print("Unshaped")
                                    reward = 0 if check_reward else -1
                                # print(reward)
                                replay_buffer.add(np.reshape(np.array([inputs, action, reward, next_inputs]), [1, 4]))

                mean_loss = []
                for opt_i in range(self.args.max_optimizations):
                    experience = replay_buffer.sample(self.args.batch_size)
                    state, action, reward, state_ = [np.squeeze(elem, axis=1) for elem in np.split(experience, 4, 1)]
                    state = np.array([ss for ss in state])
                    state_tensor = torch.tensor(state.tolist(), dtype=torch.float32)
                    state_ = np.array([ss for ss in state_])
                    state_tensor_ = torch.tensor(state_.tolist(), dtype=torch.float32)

                    reward_tensor = torch.tensor(reward.tolist(), dtype=torch.float32)
                    action_tensor = torch.tensor(action.tolist(), dtype=torch.float32)

                    if self.args.cuda:
                        state_tensor_ = state_tensor_.cuda()
                        state_tensor = state_tensor.cuda()
                        reward_tensor = reward_tensor.cuda()
                        action_tensor = action_tensor.cuda()

                    with torch.no_grad():
                        index, _ = self.q_net(state_tensor_)

                        action_index, q_values_target = self.q_target_net(state_tensor_)
                        a_matrix = np.zeros((self.args.batch_size, self.args.num_inputs))
                        a_matrix[np.arange(self.args.batch_size), action_index] = 1
                        a_matrix = torch.tensor(a_matrix, dtype=torch.float32)
                        q_values_target = torch.mul(q_values_target, a_matrix)
                        q_values_target = q_values_target.sum(1)

                        # print(q_values_target.shape)
                        # q_max = q_values[:, torch.argmax(q_values, dim=1)]
                        q_values_target = reward_tensor + self.args.gamma * q_values_target
                        q_values_target.clamp_(-1. / (1 - self.args.gamma), 0)
                        # print("#######"+str(q_values_target.shape))

                    _, q_values = self.q_net(state_tensor)
                    a_matrix = np.zeros((self.args.batch_size, self.args.num_inputs))
                    a_matrix[np.arange(self.args.batch_size), action_tensor.long()] = 1
                    a_matrix = torch.tensor(a_matrix, dtype=torch.float32)
                    q_values = torch.mul(q_values, a_matrix)
                    q_values = q_values.sum(1)

                    # print("******" + str(q_values.shape))
                    # print(q_values_target.sum())
                    # print(q_values.sum())
                    loss = (q_values_target.detach() - q_values).pow(2).sum(0)
                    self.optimizer.zero_grad()
                    loss.backward()
                    # torch.nn.utils.clip_grad_norm_(self.q_net.parameters(), 1)
                    self.optimizer.step()
                    # print(loss.detach().cpu().numpy())
                    mean_loss.append(loss.detach().cpu().numpy())

                success_rate.append(np.mean(successes))
                total_loss.append(np.mean(mean_loss))
                self.q_target_net, self.q_net = self.updateTarget(self.q_target_net, self.q_net)
                total_rewards.append(total_reward)
                ax.relim()
                ax.autoscale_view()
                ax2.relim()
                ax2.autoscale_view()
                line.set_data(np.arange(len(success_rate)), np.array(success_rate))
                # line.set_data(np.arange(len(total_rewards)), np.array(total_rewards))
                line2.set_data(np.arange(len(total_loss)), np.array(total_loss))
                fig.canvas.draw()
                fig.canvas.flush_events()
                plt.pause(1e-7)

                if episode_i % self.args.display_interval == 0:
                    torch.save(self.q_net.state_dict(), self.model_path + 'model.pt')
                # print('[{}] Epoc: {}, Cycle: {}, Episode: {}, Frames: {}, Rewards: {}'.format(datetime.now(), epoch_i,
                #                                                                              cycle_i, episode_idx,
                #                                                                               total_timesteps,
                #                                                                               reward_total))
            # self.q_target_net, self.q_net = self.updateTarget(self.q_target_net, self.q_net)
        print("Number of episodes succeeded: {}".format(succeed))
        plt.savefig(self.model_path + "stats.png")

    def _select_actions(self, x, cycle=0):
        """
        This inner method allow agent exploration. Although when HER replay is selected action exploration is not
        strictly necessary becouse the exploration is guarantee by the different goals chosen in the training loop.
        Make sure that epsilon parameters, present in arguments.py,  is set to 0 to avoid action exploration.

        Args:
            x (torch.tensor): It is the action coming from Q-network.
            cycle (int): It is the parameters used for epsilon decay. In this implementation the cycle  of the training
            is used.

        Returns:
            (numpy.array): It is the action chosen according to the action exploration policy.
        """

        if not self.args.HER:
            self.args.epsilon = 0.05 + (0.9 - 0.05) * math.exp(-1. * cycle / 200)

        with torch.no_grad():
            if np.random.rand(1) < self.args.epsilon:
                return np.array([np.random.randint(self.args.num_inputs)])
            return x.cpu().numpy()

    def test(self):
        """
        This method runs 5 Bitfip game to test what the agent learned so far. It use the model saved
        during the training so make sure to train the model before test it. In the test you can see the agent trying
        to solve 5 different tasks which means that the agent, starting from an initial state 's' and a goal 'g',
        must trying to reach to goal 'g' whit 5 different 's' and 'g'.
        """
        env = Env(self.args.num_inputs)
        model_path = self.args.save_dir + self.args.env_name + '/model.pt'
        self.q_net.load_state_dict(torch.load(model_path, map_location=lambda storage, loc: storage))
        self.q_net.eval()
        for game in range(5):
            env.reset()
            episode_experience = []
            episode_succeeded = False
            print(bcolors.ENDC + "New game...")
            input()
            print(bcolors.WARNING + "GOAL\t \t \t " + str(env.target))
            print(bcolors.OKBLUE + "START\t \t \t " + str(env.state))
            attempt = 0
            while True:
                attempt += 1
                state = np.copy(env.state)
                goal = np.copy(env.target)
                inputs = np.concatenate([state, goal], axis=-1)
                inputs_tensor = torch.tensor(inputs.tolist(), dtype=torch.float32).unsqueeze(0)
                if self.args.cuda:
                    inputs_tensor = inputs_tensor.cuda()
                with torch.no_grad():
                    action, _ = self.q_net(inputs_tensor)

                # select the actions
                time.sleep(1)
                action = self._select_actions(action, 0)
                action = action[0]
                # print(action)
                # step
                # print('action\t'+str(action))
                # print("GAOL\t"+str(goal))
                state_, reward = env.step(action)
                # print("ACHIEVED\t"+str(state_))
                # episode_experience.append((state, action, reward, state_, goal))
                # print(reward)
                # total_reward += reward
                # print(reward)
                if reward == 0:
                    print(bcolors.OKGREEN + "SUCCESS\t \t \t " + str(state_))
                    break
                else:
                    # print("FLIPPED BIT NUMBER {}".format(action))
                    print(bcolors.FAIL + "ATTEMPT NUMBER {}\t {}".format(attempt, state_))
                    # print(state_)
