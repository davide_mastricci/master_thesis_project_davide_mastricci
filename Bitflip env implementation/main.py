from agent import Agent
from arguments import get_args

if __name__ == '__main__':
    args = get_args()
    a = Agent(args)
    a.train()
