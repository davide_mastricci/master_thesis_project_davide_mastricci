# credits: https://github.com/localminimum/hindsight-experience-replay/blob/master/HER.py

import numpy as np


# Bit flipping environment
class Env():

    def __init__(self, size=15, shaped_reward=False):
        """
        This class implements the bitfilp game described into Hindsight Experience Replay.
        Credits for the implementation: https://github.com/localminimum/hindsight-experience-replay/blob/master/HER.py
        This implementation follows the schema proposed by OpenAI in their environment, which means that provides
        methods 'step' and 'reset'.

        Args:
            size (int): It's the length of the bits string. It must be a positive integer, whatever else integers are
            accepted but they lead to exceptions or unexpected behaviors. The default value is 15.

            shaped_reward (bool): Whatever the environment use the unshaped binary reward function or the shaped reward
            function. For the use of the Hindsight Experience Replay 'shaped_reward' = False. The shaped reward
            isn't tested and can lead to some unexpected behaviors. The default value is False.

        Attrs:
            state (numpy.array): the bits string representing the current state.
            target (numpy.array): the bits string representing the goal to achieve.
        """
        self.size = size
        self.shaped_reward = shaped_reward
        self.state = np.random.randint(2, size=size)
        self.target = np.random.randint(2, size=size)
        while np.sum(self.state == self.target) == size:
            self.target = np.random.randint(2, size=size)

    def step(self, action):
        """
        This method allows performing a step inside the environment given the 'action'. This action changes one
        bit of the bits string accordingly to the index specified by 'action' parameter. As result this method
        returns the reward and the new state of the environment, which is the bits string with the change you asked for.

        Args:
            action (int): The index of the bit you want to change. It must be in the range of [0, length of the string].

        Returns:
            'new_state' (numpy.array): is the the bits string with the change you asked for.
            'reward' (integer or float): The reward depends on the type of reward function the class is using. For
            unshaped reward function ('shaped_reward' = False) it will be 0 if 'new_state' is equal to the class
            attribute 'target', meaning you achieved the goal, and -1 otherwise.
            For shaped reward function ('shaped_reward' = True) this method will return a float number.

        """
        self.state[action] = 1 - self.state[action]
        if self.shaped_reward:
            return np.copy(self.state), -np.sum(np.square(self.state - self.target))
        else:
            if not np.sum(self.state == self.target) == self.size:
                return np.copy(self.state), -1
            else:
                return np.copy(self.state), 0

    def reset(self):
        """
        This method resets the environment, which means it creates a new goal (or rather a new 'target') and a new
        initial 'state'.
        """
        size = self.size
        self.state = np.random.randint(2, size=size)
        self.target = np.random.randint(2, size=size)
