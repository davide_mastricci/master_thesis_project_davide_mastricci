import torch.nn as nn
import torch.nn.functional as F
import torch
from arguments import get_args


class DQN(nn.Module):

    def __init__(self, input_dim, action_dim):
        """
        This class implements the architecture of the network described inside Hindsight Experience Replay paper.
        It's basically a 1 layer neural network with 256 hidden units as suggested by the authors of the paper.
        For the implementation of the architecture it's used the abstract class 'torch.nn.Module ' from
        the pytorch framework.

        Args:
            input_dim (int): It's the dimension of the of the state, it the case of the bitflipping environment
            it is the length of bits string. This argument must be a positive integer.
            action_dim (int): It's the dimensions of the actions that must be taken. This argument must be a positive
            integer.
        """
        super(DQN, self).__init__()
        self.action_dim = action_dim
        self.HIDDEN_NEURONS = 256
        self.lin1 = nn.Linear(input_dim, self.HIDDEN_NEURONS)
        self.lin2 = nn.Linear(self.HIDDEN_NEURONS, action_dim, bias=False)
        nn.init.xavier_uniform_(self.lin1.weight)
        self.lin1.bias.data = torch.zeros(self.lin1.bias.data.shape[0])
        nn.init.xavier_uniform_(self.lin2.weight)


        self.cuda = get_args().cuda

    def forward(self, x):
        """
        This method perform the forward pass through the network. It works either with a single vector or with
        a batch.

        Args:
            x (torch.tensor): It's the input vector (state) or the batch (set of states). The dimension of state must
            matches with the one declared as 'input_dim'.

        Returns:
            index (torch.tensor): The index of the action which lead to the max Q-value.
            x (torch.tensor): The q-values of the each action.
        """
        x = F.relu(self.lin1(x))
        x = self.lin2(x)
        index = torch.argmax(x, 1)
        return index, x


