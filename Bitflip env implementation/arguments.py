import argparse


# Experimet parameters

def get_args():
    parse = argparse.ArgumentParser(description='dqn')
    parse.add_argument('--env-name', type=str, default='Bitflip', help='the training environment')
    parse.add_argument('--num-inputs', type=int, default=40, help='number of bit')
    parse.add_argument('--shaped-function', action='store_true', help="whatever to use shaped function or not")
    #parse.add_argument('--shaped-function', action='store_false', help="whatever to use shaped function or not")
    parse.add_argument('--gamma', type=float, default=0.98, help='discount factor')
    parse.add_argument('--tau', type=float, default=0.05, help='discount factor')
    parse.add_argument('--noise-scale', type=float, default=0.3, help='noise scale')
    parse.add_argument('--final-noise-scale', type=float, default=0.3, help='final noise scale')
    parse.add_argument('--exploration-length', type=int, default=3, help='the episode that end the exploration')
    parse.add_argument('--seed', type=int, default=123, help='the random seed')
    parse.add_argument('--batch-size', type=int, default=128, help='the batch size that sample')
    parse.add_argument('--max-steps', type=int, default=40, help='the max time steps per episode')
    parse.add_argument('--max-episode', type=int, default=64, help='the max length of episode to train the agent')
    parse.add_argument('--max-epochs', type=int, default=20, help='the max number of epochs to train the agent')
    parse.add_argument('--max-cycles', type=int, default=50, help='the max number of epochs to train the agent')
    parse.add_argument('--max-optimizations', type=int, default=40, help='the max number of optimization steps per '
                                                                         'cycle')
    parse.add_argument('--updates-per-step', type=int, default=1, help='num of update per steps')
    parse.add_argument('--replay-size', type=int, default=1e6, help='the size of replay buffer')
    parse.add_argument('--cuda', action='store_true', help='if use the GPU to do the training')
    parse.add_argument('--net-lr', type=float, default=1e-3, help='the learning rate of actor network')
    parse.add_argument('--display-interval', type=int, default=1, help='display interval')
        #    parse.add_argument('--save-dir', type=str, default='DQN+HER_unshaped/', help='the folder that save the models')
    parse.add_argument('--save-dir', type=str, default='DQN_unshaped/', help='the folder that save the models')
        #parse.add_argument('--save-dir', type=str, default='DQN+HER_shaped/', help='the folder that save the models')
#    parse.add_argument('--save-dir', type=str, default='DQN_shaped/', help='the folder that save the models')


    parse.add_argument('--epsilon', type=float, default=0.8, help='epsilon-greedy probability')

    # HER parameters
    #parse.add_argument('--HER', action='store_false', help="whatever to use HER replay or not")
    parse.add_argument('--HER', action='store_true', help="whatever to use HER replay or not")

    parse.add_argument('--k-future', type=int, default=4, help="number of future state to replay")

    # get args...
    args = parse.parse_args()

    return args
