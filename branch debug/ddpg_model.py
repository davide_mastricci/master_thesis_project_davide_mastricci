import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch
import gym
import numpy as np
from collections import namedtuple
import random
import matplotlib.pyplot as plt
from time import gmtime, strftime
import seaborn as sns


####################################################################################################
# Credits: https://github.com/ghliu/pytorch-ddpg/blob/master/normalized_env.py
# NormalizedEnv is a wrapper for the actions of the environment in  order to have a normalized action

# https://github.com/openai/gym/blob/master/gym/core.py

class NormalizedEnv(gym.ActionWrapper):
    """ Wrap action """

    def _action(self, action):
        act_k = (self.action_space.high - self.action_space.low) / 2.
        act_b = (self.action_space.high + self.action_space.low) / 2.
        return act_k * action + act_b

    def _reverse_action(self, action):
        act_k_inv = 2. / (self.action_space.high - self.action_space.low)
        act_b = (self.action_space.high + self.action_space.low) / 2.
        return act_k_inv * (action - act_b)


# Creating environment
env = NormalizedEnv(gym.make("Pendulum-v0"))
env.reset()

device = torch.device("cpu")
# device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

STATE_DIM = env.observation_space.shape[0]
ACTION_DIM = env.action_space.shape[0]

######################################################################
# Replay Memory
# -------------
#
# We'll be using experience replay memory for training our DDPG. It stores
# the transitions that the agent observes , allowing us to reuse this data
# later. By sampling from it randomly, the transitions that build up a
# batch are decorrelated.
#
# For this, we're going to need two classes:
#
# -  ``Transition`` - a named tuple representing a single transition in
#    our environment
# -  ``ReplayMemory`` - a cyclic buffer of bounded size that holds the
#    transitions observed recently. It also implements a ``.sample()``
#    method for selecting a random batch of transitions for training.
#
# The following approach is inspired by https://pytorch.org/tutorials/intermediate/reinforcement_q_learning.html

Transition = namedtuple('Transition',
                        ('state', 'action', 'next_state', 'reward'))


class ReplayMemory(object):

    def __init__(self, capacity):
        self.capacity = capacity
        self.memory = []
        self.position = 0

    def push(self, *args):
        """ Saves a transition.

            Args:
                *args : state_goal, action, reward, next_state_goal, episode, done"""
        if len(self.memory) < self.capacity:
            self.memory.append(None)
        self.memory[self.position] = Transition(*args)
        self.position = int((self.position + 1) % self.capacity)

    def sample(self, batch_size):
        """Sample a batch of transition form the replay mamory.

            Args:
                batch_size (int): number of element to sample

            Returns: ...
                """

        return random.sample(self.memory, batch_size)

    def __len__(self):
        return len(self.memory)


######################################################################
# DDPG Model (Actor-Critic) classes
# -------------
#


class Critic(nn.Module):

    def __init__(self):
        super(Critic, self).__init__()
        # Network architecture
        self.HIDDEN_DIM = 64
        self.lin1 = nn.Linear(STATE_DIM + ACTION_DIM, self.HIDDEN_DIM)
        self.lin2 = nn.Linear(self.HIDDEN_DIM, self.HIDDEN_DIM)
        self.lin3 = nn.Linear(self.HIDDEN_DIM, self.HIDDEN_DIM)
        self.lin4 = nn.Linear(self.HIDDEN_DIM, self.HIDDEN_DIM)
        self.lin5 = nn.Linear(self.HIDDEN_DIM, 1)

    def forward(self, x):
        self.scaler.observe(x)
        x = self.scaler.normalize(x).detach()
        x = self.lin1(x)
        x = F.relu(self.lin2(x))
        x = F.relu(self.lin3(x))
        x = F.relu(self.lin4(x))
        x = self.lin5(x)
        return x


class Actor(nn.Module):

    def __init__(self):
        super(Actor, self).__init__()
        # Network architecture
        self.HIDDEN_DIM = 64
        self.lin1 = nn.Linear(STATE_DIM, self.HIDDEN_DIM)
        self.lin2 = nn.Linear(self.HIDDEN_DIM, self.HIDDEN_DIM)
        self.lin3 = nn.Linear(self.HIDDEN_DIM, self.HIDDEN_DIM)
        self.lin4 = nn.Linear(self.HIDDEN_DIM, self.HIDDEN_DIM)
        self.lin5 = nn.Linear(self.HIDDEN_DIM, ACTION_DIM)

    def forward(self, x):
        x = self.lin1(x)
        x = F.relu(self.lin2(x))
        x = F.relu(self.lin3(x))
        x = F.relu(self.lin4(x))
        x = F.tanh(self.lin5(x))
        x = torch.clamp(x, min=-0.05, max=0.05)
        return x

    def get_preacitvation(self, x):
        """Get the activiation before the tanh pass.

            Args:
                x (torch.Tensor): input for the network"""
        x = F.relu(self.lin2(x))
        x = F.relu(self.lin3(x))
        x = F.relu(self.lin4(x))
        return x


env.reset()





###################################################################
# Training Hyperparameters
# -----------------------
#
#

BATCH_SIZE = 128
EXPLORATION_PROBABILITY = 0.2
# Decay coefficient used to update target networks' weights during the training
DECAY_COEF = 0.95
LR_ADAM = 0.001
REPLAY_BUFFER_CAPACITY = 10e6
# Discount factor for Bellman equation used in critic (Q) loss function
GAMMA = 0.98


def select_action(state, mu):
    """

    :param state:
    :param mu:
    :return: action
    """
    with torch.no_grad():
        if random.random() > EXPLORATION_PROBABILITY:
            # if torch.cuda.is_available():
            #     state = state.cuda()
            return mu(state).cpu()
        else:
            return torch.tensor(env.action_space.sample())


def soft_update(target, source, decay_coeff):
    """
    :param target:
    :param source:
    :param decay_coeff:
    :return: target, source
    """
    for target_param, param in zip(target.parameters(), source.parameters()):
        target_param.data.copy_(
            target_param.data * (1.0 - decay_coeff) + param.data * decay_coeff
        )

    return  target, source

def optimize_model(replay_buffer):
    if len(replay_buffer) < BATCH_SIZE:
        return
    transitions = replay_buffer.sample(BATCH_SIZE)
    batch = Transition(*zip(*transitions))

    state_goal_batch = torch.stack(batch.state_goal)
    action_batch = torch.stack(batch.action)
    reward_batch = torch.stack(batch.reward)
    next_state_goal_batch = torch.stack(batch.next_state_goal)

    # Prepare for the target q batch
    next_q_values = self.critic_target([
        to_tensor(next_state_batch, volatile=True),
        self.actor_target(to_tensor(next_state_batch, volatile=True)),
    ])
    next_q_values.volatile = False

    target_q_batch = to_tensor(reward_batch) + \
                     self.discount * to_tensor(terminal_batch.astype(np.float)) * next_q_values

    # Critic update
    self.critic.zero_grad()

    q_batch = self.critic([to_tensor(state_batch), to_tensor(action_batch)])

    value_loss = criterion(q_batch, target_q_batch)
    value_loss.backward()
    self.critic_optim.step()

    # Actor update
    self.actor.zero_grad()

    policy_loss = -self.critic([
        to_tensor(state_batch),
        self.actor(to_tensor(state_batch))
    ])

    policy_loss = policy_loss.mean()
    policy_loss.backward()
    self.actor_optim.step()

    # Target update
    soft_update(self.actor_target, self.actor, self.tau)
    soft_update(self.critic_target, self.critic, self.tau)