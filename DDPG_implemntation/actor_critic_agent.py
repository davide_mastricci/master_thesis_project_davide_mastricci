import numpy as np
import torch
from networks import Actor, Critic
from noise import OUNoise
import random
from datetime import datetime
import os
from normalize import Normalizer
import matplotlib.pyplot as plt
import seaborn as sns

# from https://github.com/TianhongDai/reinforcement-learning-algorithms/blob/master/03-deep-deterministic-policy-gradient/ddpg_agent.py

class Agent:
    def __init__(self, args, env):
        self.args = args
        self.env = env
        # get the number of inputs...
        num_inputs = self.env.observation_space.spaces['observation'].shape[0] + self.env.observation_space.spaces['desired_goal'].shape[0]
        num_actions = self.env.action_space.shape[0]
        self.action_scale = self.env.action_space.high[0]
        # setting up the input normalizer
        self.normalizer = Normalizer(num_inputs)
        # build up the network
        self.actor_net = Actor(num_inputs, num_actions)
        self.critic_net = Critic(num_inputs, num_actions)
        # get the target network...
        self.actor_target_net = Actor(num_inputs, num_actions)
        self.critic_target_net = Critic(num_inputs, num_actions)
        if self.args.cuda:
            self.actor_net.cuda()
            self.critic_net.cuda()
            self.actor_target_net.cuda()
            self.critic_target_net.cuda()
        # copy the parameters..
        self.actor_target_net.load_state_dict(self.actor_net.state_dict())
        self.critic_target_net.load_state_dict(self.critic_net.state_dict())
        # setup the optimizer...
        self.optimizer_actor = torch.optim.Adam(self.actor_net.parameters(), lr=self.args.actor_lr)
        self.optimizer_critic = torch.optim.Adam(self.critic_net.parameters(), lr=self.args.critic_lr,
                                                 weight_decay=self.args.critic_l2_reg)
        # setting up the noise
        self.ou_noise = OUNoise(num_actions)
        # check some dir
        if not os.path.exists(self.args.save_dir):
            os.mkdir(self.args.save_dir)
        self.model_path = self.args.save_dir + self.args.env_name + '/'
        if not os.path.exists(self.model_path):
            os.mkdir(self.model_path)

    # start to train the network..
    def train(self):
        # init the brain memory
        replay_buffer = []
        episode_buffer = []
        total_timesteps = 0
        running_reward = None
        reward_buffer = []
        for epoch_i in range(self.args.max_epochs):
            reward_total = 0
            for cycle_i in range(self.args.max_cycles):

                for episode_idx in range(self.args.max_episode):
                    state = self.env.reset()
                    goal = state['desired_goal']
                    state = state['observation']
                    # get the scale of the ou noise...
                    self.ou_noise.scale = (self.args.noise_scale - self.args.final_noise_scale) * max(0,
                                                                                                      self.args.exploration_length - episode_idx) / \
                                          self.args.exploration_length + self.args.final_noise_scale
                    self.ou_noise.reset()
                    # start the training
                    reward_total_per_episode = 0
                    episode_buffer = []
                    for time_step_i in range(self.args.max_steps):
                        state_tensor = np.array(state.tolist() + goal.tolist())
                        state_tensor = self.normalizer.observe_normalize(state_tensor)
                        state_tensor = torch.tensor(state_tensor, dtype=torch.float32).unsqueeze(0)
                        if self.args.cuda:
                            state_tensor = state_tensor.cuda()
                        with torch.no_grad():
                            policy = self.actor_net(state_tensor)
                        # start to select the actions...
                        actions = self._select_actions(policy)
                        # step
                        state_, reward, done, info = self.env.step(actions * self.action_scale)
                        achieved_goal = state_['achieved_goal']
                        state_ = state_['observation']
                        total_timesteps += 1
                        # reward_total += 1 if reward == 0 else 0
                        reward_total_per_episode += reward
                        # start to store the samples...
                        to_store_state = np.array(state.tolist() + goal.tolist())
                        to_store_state = self.normalizer.normalize(to_store_state)
                        to_store_state_ = np.array(state_.tolist() + goal.tolist())
                        to_store_state_ = self.normalizer.observe_normalize(to_store_state_)
                        replay_buffer.append((to_store_state, reward, actions, done, to_store_state_))
                        episode_buffer.append((state, reward, actions, done, state_, info, goal, achieved_goal))
                        # check if the buffer size is outof range
                        if len(replay_buffer) > self.args.replay_size:
                            replay_buffer.pop(0)
                        # if len(replay_buffer) > self.args.batch_size:
                        #     mini_batch = random.sample(replay_buffer, self.args.batch_size)
                        #     # start to update the network
                        #     _, _ = self._train(mini_batch)
                        if done:
                            episode_buffer = []
                            break
                        state = state_
                    # running_reward = reward_total if running_reward is None else running_reward * 0.99 + reward_total * 0.01
                    last_achieved_goal  = achieved_goal
                    for i in range(len(episode_buffer)):
                        state, reward, actions, done, state_, info, goal, achieved_goal = episode_buffer[i]
                        reward = self.env.compute_reward(achieved_goal, last_achieved_goal, info)
                        done = True if reward == 0 else False

                        to_store_state = np.array(state.tolist() + last_achieved_goal.tolist())
                        to_store_state = self.normalizer.normalize(to_store_state)
                        to_store_state_ = np.array(state_.tolist() + last_achieved_goal.tolist())
                        to_store_state_ = self.normalizer.normalize(to_store_state_)

                        replay_buffer.append((to_store_state, reward, actions, done, to_store_state_))
                        if len(replay_buffer) > self.args.replay_size:
                            replay_buffer.pop(0)
                    if len(replay_buffer) > self.args.batch_size:
                        mini_batch = random.sample(replay_buffer, self.args.batch_size)
                        # start to update the network
                        for i in range(40):
                            _, _ = self._train(mini_batch)

                    running_reward = reward_total if running_reward is None else running_reward * 0.99 + reward_total * 0.01


                    if episode_idx % self.args.display_interval == 0:
                        torch.save(self.actor_net.state_dict(), self.model_path + 'model.pt')
                        print('[{}] Epoc: {}, Cycle: {}, Episode: {}, Frames: {}, Rewards: {}'.format(datetime.now(), epoch_i, cycle_i, episode_idx, time_step_i,
                                                                                 reward_total_per_episode))

                        self.normalizer.serialize(self.args.save_dir + self.args.env_name + 'normalizer.pickle')

                # updating the target network after every cycle the
                self._soft_update_target_network(self.critic_target_net, self.critic_net)
                self._soft_update_target_network(self.actor_target_net, self.actor_net)

            self.env.close()
            reward_buffer.append(reward_total)
            self._plot_progress(reward_buffer)

    # select actions
    def _select_actions(self, policy):
        if random.random() > 0.3:
            GOAL_HIGHER_LIMIT = 1
            GOAL_LOWER_LIMIT = -1
            points = np.linspace(GOAL_LOWER_LIMIT, GOAL_HIGHER_LIMIT)
            mean = 0 #points.mean()
            std = 0.05 #points[1]  # 5% of all possible value for x

            actions = policy.detach().cpu().numpy()[0]
            actions = actions + np.random.normal(mean, std, actions.shape[0])
            actions = np.clip(actions, -1, 1)
        else:
            actions = self.env.action_space.sample()
        return actions

    # update the network
    def _train(self, mini_batch):
        state_batch = np.array([element[0] for element in mini_batch])
        state_batch = torch.tensor(state_batch, dtype=torch.float32)
        # reward batch
        reward_batch = np.array([element[1] for element in mini_batch])
        reward_batch = torch.tensor(reward_batch, dtype=torch.float32).unsqueeze(1)
        # done batch
        done_batch = np.array([int(element[3]) for element in mini_batch])
        done_batch = 1 - done_batch
        done_batch = torch.tensor(done_batch, dtype=torch.float32).unsqueeze(1)
        # action batch
        actions_batch = np.array([element[2] for element in mini_batch])
        actions_batch = torch.tensor(actions_batch, dtype=torch.float32)
        # next stsate
        state_next_batch = np.array([element[4] for element in mini_batch])
        state_next_batch = torch.tensor(state_next_batch, dtype=torch.float32)

        # check if use the cuda
        if self.args.cuda:
            state_batch = state_batch.cuda()
            reward_batch = reward_batch.cuda()
            done_batch = done_batch.cuda()
            actions_batch = actions_batch.cuda()
            state_next_batch = state_next_batch.cuda()

        # update the critic network...
        with torch.no_grad():
            actions_out = self.actor_target_net(state_next_batch)
            expected_q_value = self.critic_target_net(state_next_batch, actions_out)
        # get the target value
        target_value = reward_batch + self.args.gamma * expected_q_value # * done_batch
        target_value.clamp_(-1 / (1 - self.args.gamma), 0)
        target_value = target_value.detach()
        values = self.critic_net(state_batch, actions_batch)
        critic_loss = (target_value - values).pow(2).mean()
        self.optimizer_critic.zero_grad()
        critic_loss.backward()
        self.optimizer_critic.step()

        torch.nn.utils.clip_grad_norm_(self.critic_net.parameters(), 1)

        # start to update the actor network
        actor_loss = -self.critic_net(state_batch, self.actor_net(state_batch)).mean()
        self.optimizer_actor.zero_grad()
        actor_loss.backward()
        self.optimizer_actor.step()

        torch.nn.utils.clip_grad_norm_(self.actor_net.parameters(), 1)

        # then, start to softupdate the network...
        # self._soft_update_target_network(self.critic_target_net, self.critic_net)
        # self._soft_update_target_network(self.actor_target_net, self.actor_net)

        return actor_loss.item(), critic_loss.item()

    # soft update the network
    def _soft_update_target_network(self, target, source):
        # update the critic network firstly...
        for target_param, param in zip(target.parameters(), source.parameters()):
            target_param.data.copy_(self.args.tau * param.data + (1 - self.args.tau) * target_param.data)

    # functions to test the network
    def show_policy_learned(self):
        model_path = self.args.save_dir + self.args.env_name + '/model.pt'
        self.actor_net.load_state_dict(torch.load(model_path, map_location=lambda storage, loc: storage))
        self.actor_net.eval()

        normalizer_path = self.args.save_dir + self.args.env_name + 'normalizer.pickle'
        self.normalizer = Normalizer(10)
        self.normalizer.deserialize(normalizer_path)

        # start to test
        for _ in range(5):
            state = self.env.reset()
            goal = state['desired_goal']
            state = state['observation']
            reward_sum = 0
            while True:
                self.env.render()
                state = np.array(state.tolist() + goal.tolist())
                self.normalizer.normalize(state)
                state = torch.tensor(state, dtype=torch.float32).unsqueeze(0)
                with torch.no_grad():
                    actions = self.actor_net(state)
                actions = actions.detach().numpy()[0]
                state_, reward, done, _ = self.env.step(actions)
                state_ = state_['observation']
                reward_sum += reward
                if done:
                    break
                state = state_
            print('The reward of this episode is {}.'.format(reward_sum))
        self.env.close()

    def _plot_progress(self, rewads):
        if len(rewads) < 2:
            return
        plt.clf()
        sns.set(style="ticks", context="talk")
        #    plt.style.use("dark_background")
        y = np.array(rewads).reshape((1, -1))
        ax = sns.tsplot(data=y, ci="sd")
        mean = y.mean()
        std = y.std()
        # ax.errorbar([i for i in range(y.shape[1])], mean, yerr=std, fmt='-o')
        ax.set(xlabel='EPOCS', ylabel='% SUCCESS RATE')
        plt.savefig(self.model_path + 'learning_curve.png')
        plt.close()
