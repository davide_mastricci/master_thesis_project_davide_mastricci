import numpy as np


# From https://github.com/TianhongDai/reinforcement-learning-algorithms/blob/master/ddpg/ounoise.py
# https://en.wikipedia.org/wiki/Ornstein–Uhlenbeck_process

class OUNoise:

    def __init__(self, action_dim, scale=0.1, mu=0, theta=0.15, sigma=0.2):
        self.action_dim = action_dim
        self.scale = scale
        self.mu = mu
        self.theta = theta
        self.sigma = sigma
        self.state = np.ones(self.action_dim) * self.mu
        self.reset()



    def reset(self):
        self.state = np.ones(self.action_dim) * self.mu

    def noise(self):
        x = self.state
        dx = self.theta * (self.mu - x) + self.sigma * np.random.rand(len(x))
        self.state = x + dx
        return  self.state * self.scale

