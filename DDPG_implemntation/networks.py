import torch
import torch.nn as nn
import torch.nn.functional as F


class Critic(nn.Module):

    def __init__(self, input_dim, action_dim):
        super(Critic, self).__init__()
        self.HIDDEN_NEURONS = 256
        self.lin1 = nn.Linear(input_dim, self.HIDDEN_NEURONS)
        self.lin2 = nn.Linear(self.HIDDEN_NEURONS + action_dim, self.HIDDEN_NEURONS)
        self.lin3 = nn.Linear(self.HIDDEN_NEURONS, 1)

        nn.init.uniform_(self.lin3.weight, -3e-3, 3e-3)
        nn.init.uniform_(self.lin3.bias, -3e-3, 3e-3)

    def forward(self, x, actions):
        x = F.relu(self.lin1(x))
        x = torch.cat((x, actions), 1)
        x = F.relu(self.lin2(x))
        x = self.lin3(x)

        return x


class Actor(nn.Module):

    def __init__(self, input_dim, action_dim):
        super(Actor, self).__init__()
        self.HIDDEN_NEURONS = 256
        self.lin1 = nn.Linear(input_dim, self.HIDDEN_NEURONS)
        self.lin2 = nn.Linear(self.HIDDEN_NEURONS, self.HIDDEN_NEURONS)
        self.lin3 = nn.Linear(self.HIDDEN_NEURONS, action_dim)

        nn.init.uniform_(self.lin3.weight, -3e-3, 3e-3)
        nn.init.uniform_(self.lin3.bias, -3e-3, 3e-3)

    def forward(self, x):
        x = F.relu(self.lin1(x))
        x = F.relu(self.lin2(x))
        x = F.tanh(self.lin3(x))

        return x
