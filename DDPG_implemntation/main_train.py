from arguments import get_args
from actor_critic_agent import Agent
import gym


if __name__ == '__main__':
    args = get_args()
    env = gym.make(args.env_name)
    ddpg_agent = Agent(args, env)
    ddpg_agent.train()