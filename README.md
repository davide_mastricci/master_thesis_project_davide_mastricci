# Hindsight Experience Replay (HER)
---

This is the implementation of **HER** using the **Bitflipping envirorment** mentioned in this [paper](https://arxiv.org/abs/1707.01495), using the **k-future** strategy. The algorithm is implemented using *Python 3* and *PyTorch 0.4*.

## To train
To train the agent type
```shell
$ python  python Bitflip\ env\ implementation/main.py
```
from bash.
The model and plots can be found in *Bitflip env implementation/[save dir]/*, where **[save dir]** is specified into *Bitflip env implementation/arguments.py*.


## To test
To test what the agent learned so far type
```shell
$ python Bitflip\ env\ implementation/test.py
```
from bash.
Make sure you already train the agent because this script use the model saved during the training. For the optimal result wait till the end of the training before running the test.
The test consist of choosing **5** times an initial bits string (a.k.a. **initial state**) and a target bits string (a.k.a. **goal**) and the agent has to reach the goal starting from the initial state.

## Results
Following it is shown the result for DQN+HER with unshaped reward using k-future strategy:
![results](Bitflip%20env%20implementation/DQN%2BHER_unshaped/Bitflip/stats.png)

### Acknowledgment
Some parts of the code are borrowed from [here](https://github.com/localminimum/hindsight-experience-replay).
