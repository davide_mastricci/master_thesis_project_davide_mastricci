import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch
import gym
import numpy as np
from collections import namedtuple
import random
import matplotlib.pyplot as plt
from time import gmtime, strftime
import seaborn as sns
from PIL import Image

# import pdb

# Creating environment
env = gym.make('FetchPush-v1')

# Making code compatible with cuda GPU
# device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
device = torch.device("cpu")

# These following string constant are useful to extract the data from the observation returned by the envirorment
ACHIEVED_GOAL = 'achieved_goal'
DESIRED_GOAL = 'desired_goal'
STATE = 'observation'

# DIMENSION OF THE ENVIRONMENT
# In the general approach of HER the state is combined with the goal
STATE_GOAL_DIM = env.observation_space.spaces[STATE].shape[0] + env.observation_space.spaces[DESIRED_GOAL].shape[0]
ACTION_DIM = env.action_space.shape[0]

######################################################################
# Replay Memory
# -------------
#
# We'll be using experience replay memory for training our DDPG. It stores
# the transitions that the agent observes , allowing us to reuse this data
# later. By sampling from it randomly, the transitions that build up a
# batch are decorrelated.
#
# For this, we're going to need two classes:
#
# -  ``Transition`` - a named tuple representing a single transition in
#    our environment
# -  ``ReplayMemory`` - a cyclic buffer of bounded size that holds the
#    transitions observed recently. It also implements a ``.sample()``
#    method for selecting a random batch of transitions for training.
#
# The following approach is inspired by https://pytorch.org/tutorials/intermediate/reinforcement_q_learning.html

Transition = namedtuple('Transition',
                        ('state_goal', 'action', 'next_state_goal', 'reward', 'info', 'time_step'))


class ReplayMemory(object):

    def __init__(self, capacity):
        self.capacity = capacity
        self.memory = []
        self.position = 0

    def push(self, *args):
        """ Saves a transition.

            Args:
                *args : state_goal, action, reward, next_state_goal, episode, done"""
        if len(self.memory) < self.capacity:
            self.memory.append(None)
        self.memory[self.position] = Transition(*args)
        self.position = int((self.position + 1) % self.capacity)

    def sample(self, batch_size):
        """Sample a batch of transition form the replay mamory.

            Args:
                batch_size (int): number of element to sample

            Returns: ...
                """

        return random.sample(self.memory, batch_size)

    def __len__(self):
        return len(self.memory)


######################################################################
# DDPG Model (Actor-Critic) classes
# -------------
#


class Critic(nn.Module):

    def __init__(self):
        super(Critic, self).__init__()
        # Network architecture
        self.HIDDEN_DIM = 64  # paper values
        #self.HIDDEN_DIM = 256  # experiment value https://github.com/openai/baselines/blob/master/baselines/her/experiment/config.py
        self.lin1 = nn.Linear(STATE_GOAL_DIM + ACTION_DIM, self.HIDDEN_DIM)
        self.lin2 = nn.Linear(self.HIDDEN_DIM, self.HIDDEN_DIM)
        self.lin3 = nn.Linear(self.HIDDEN_DIM, 1)
        self.batch1 = nn.BatchNorm1d(STATE_GOAL_DIM + ACTION_DIM)
        self.batch2 = nn.BatchNorm1d(self.HIDDEN_DIM)
        self.batch3 = nn.BatchNorm1d(self.HIDDEN_DIM)
        self.batch4 = nn.BatchNorm1d(1)
        # self.scaler = Normalizer(STATE_GOAL_DIM + ACTION_DIM)

    def forward(self, x):
        # self.scaler.observe(x)
        # x = self.scaler.normalize(x).detach()
        # x = x.detach()
        # x.requires_grad = True
        if x.dim() != 1:
            x = self.batch1(x)
        x = torch.clamp(x, min=-5, max=5)
        x = F.relu(self.lin1(x))
        if x.dim() != 1:
            x = self.batch2(x)
        x = F.relu(self.lin2(x))
        if x.dim() != 1:
            x = self.batch3(x)
        x = F.relu(self.lin3(x))
        if x.dim() != 1:
            x = self.batch4(x)
        return x


class Actor(nn.Module):

    def __init__(self):
        super(Actor, self).__init__()
        # Network architecture
        self.HIDDEN_DIM = 64  # paper values
        #self.HIDDEN_DIM = 256  # experiment values
        self.lin1 = nn.Linear(STATE_GOAL_DIM, self.HIDDEN_DIM)
        self.lin2 = nn.Linear(self.HIDDEN_DIM, self.HIDDEN_DIM)
        self.lin3 = nn.Linear(self.HIDDEN_DIM, ACTION_DIM)
        self.batch1 = nn.BatchNorm1d(STATE_GOAL_DIM)
        self.batch2 = nn.BatchNorm1d(self.HIDDEN_DIM)
        self.batch3 = nn.BatchNorm1d(self.HIDDEN_DIM)
        self.preactivation = None
        # self.scaler = Normalizer(STATE_GOAL_DIM)

    def forward(self, x):
        if x.dim() != 1:
            x = self.batch1(x)
        x = torch.clamp(x, min=-5, max=5)
        x = F.relu(self.lin1(x))
        if x.dim() != 1:
            x = self.batch2(x)
        x = F.relu(self.lin2(x))
        if x.dim() != 1:
            x = self.batch3(x)
        self.preactivation = torch.tensor(x.data, dtype=torch.double)
        x = F.tanh(self.lin3(x))
        x = torch.clamp(x, min=-0.05, max=0.05)
        return x

    def get_preacitvation(self, x):
        """Get the activiation before the tanh pass.

            Args:
                x (torch.Tensor): input for the network"""
        # print("DIM {}".format(x.dim()))
        # if x.dim() != 1:
        #     x = self.batch1(1)
        # x = torch.clamp(x, min=-5, max=5)
        # x = F.relu(self.lin1(x))
        # if x.dim() != 1:
        #     x = self.batch2(x)
        # x = F.relu(self.lin2(x))
        # if x.dim() != 1:
        #     x = self.batch3(x)
        return self.preactivation


env.reset()

######################################################################
# Training
# --------
#
# Hyperparameters and utilities
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# The following section instantiates our networks and their optimizer, and defines some
# utilities:
#
# -  ``select_action`` - will select an action accordingly to an epsilon
#    greedy policy.The probability of choosing a random action will be at
#    ``EXPLORATION_PROBABILITY`` .
#
# -  ``updateTarget`` - will update target (stable) network with training networks's weights.
#
# -  ``future`` - will extract random states (``num_states``) from the same episode as the transition begin
#      replayed and were observed after it.

BATCH_SIZE = 128
MAX_EPOCS = 200
MAX_CYCLES = 50
MAX_EPISODES = 16
MAX_TIME_STEPS = 50
MAX_OPTIMIZATION_STEPS = 40
EXPLORATION_PROBABILITY = 0.2
# Decay coefficient used to update target networks' weights during the training
DECAY_COEFF = 0.95
LR_ADAM = 0.001
REPLAY_BUFFER_CAPACITY = 10e6
# Discount factor for Bellman equation used in critic (Q) loss function
GAMMA = 0.98
# NUmber of virtual goal to replay
NUM_GOALS = 8


def select_action(state):
    global mu
    with torch.no_grad():
        if random.random() > EXPLORATION_PROBABILITY:
            # if torch.cuda.is_available():
            #     state = state.cuda()
            return mu(state)
        else:
            return torch.tensor(env.action_space.sample(), dtype=torch.double)


def updateTarget(train, target, decay_coeff=0.95):
    """Update Target netwotk with training network parameters

        Args:
            train (torch.nn.Module): training network
            target (torch.nn.Module): target network
            decay_coeff (float64): decay coefficent applied to the target network

        Returns:
            train (torch.nn.Module): training network
            target (torch.nn.Module): target network
    """

    for target_param, param in zip(target.parameters(), train.parameters()):
        target_param.data.copy_(target_param.data * (1.0 - decay_coeff) + param.data * decay_coeff)

    return train, target


State_Action = namedtuple('State_Action', ('state', 'action', 'next_state', 'achived_goal', 'info', 'time_step'))


def future(time_step, state_action_pairs, num_goals=NUM_GOALS):
    future_states = []
    for i in range(time_step + 1, MAX_TIME_STEPS):
        future_states.append(state_action_pairs[t].achived_goal)

    if len(future_states) <= num_goals:
        return future_states
    else:
        return random.choices(future_states, k=num_goals)


class Normalizer():
    """Normalize the input for the neural networks.
        Credits : https://discuss.pytorch.org/t/normalization-of-input-data-to-qnetwork/14800/2
        https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Online_algorithm
        """

    def __init__(self, num_inputs):
        self.n = torch.zeros(num_inputs).to(device)
        self.mean = torch.zeros(num_inputs).to(device)
        self.mean_diff = torch.zeros(num_inputs).to(device)
        self.var = torch.zeros(num_inputs).to(device)

    def observe(self, x):
        if len(x) > 1:
            for i in range(len(x)):
                self.n += 1.
                last_mean = self.mean.clone()
                x_i = x[i].clone()
                self.mean += (x_i - self.mean) / self.n
                self.mean_diff += (x_i - last_mean) * (x_i - self.mean)
                self.var = torch.clamp(self.mean_diff / self.n, min=1e-2)
        else:
            self.n += 1.
            last_mean = self.mean.clone()
            x = x.clone()
            self.mean += (x - self.mean) / self.n
            self.mean_diff += (x - last_mean) * (x - self.mean)
            self.var = torch.clamp(self.mean_diff / self.n, min=1e-2)

        last_mean = None
        x_i = None
        x = None

    def normalize(self, inputs, min=-5, max=5):
        obs_std = torch.sqrt(self.var)
        obs_std = (inputs - self.mean) / obs_std
        obs_std = obs_std.clamp(min, max)
        return obs_std


mu = Actor().to(device)
mu_target = Actor().to(device)
mu_target.load_state_dict(mu.state_dict())
mu_target.eval()
mu.train()
Q = Critic().to(device)
Q_target = Critic().to(device)
Q_target.load_state_dict(Q.state_dict())
Q_target.eval()
Q.train()

mu.double()
mu_target.double()
Q.double()
Q_target.double()

mu_optimizer = optim.Adam(mu.parameters(), lr=LR_ADAM)
Q_optimizer = optim.Adam(Q.parameters(), lr=LR_ADAM)

loss_critic = torch.nn.MSELoss()
replay_buffer = ReplayMemory(REPLAY_BUFFER_CAPACITY)


def logg(model, path):
    file = open(path, "w")
    file.close()
    for name, param in model.named_parameters():
        if param.requires_grad:
            print("{}\t{}".format(name, str(param.data)), file=open(path, "a"))


logg(mu, "logg_parameters/mu_parameters.txt")
logg(Q, "logg_parameters/Q_parameters.txt")
logg(mu_target, "logg_parameters/mu_target_parameters.txt")
logg(Q_target, "logg_parameters/Q_target_parameters.txt")


######################################################################
# Training loop
# ^^^^^^^^^^^^^
#

def optimize_model():
    global mu
    global mu_target
    global mu_optimizer
    global Q
    global Q_target
    global Q_optimizer
    global replay_buffer
    global loss_critic

    if len(replay_buffer) < BATCH_SIZE:
        return
    transitions = replay_buffer.sample(BATCH_SIZE)
    batch = Transition(*zip(*transitions))

    state_goal_batch = torch.stack(batch.state_goal)
    action_batch = torch.stack(batch.action)
    reward_batch = torch.stack(batch.reward)
#    print("Percentage of zero = {} %".format((128 - torch.nonzero(reward_batch).size(0)) / 128 * 100))
    next_state_goal_batch = torch.stack(batch.next_state_goal)
    if torch.isnan(state_goal_batch).sum() > 0 or torch.isnan(action_batch).sum() > 0 or torch.isnan(
            reward_batch).sum() > 0 or torch.isnan(next_state_goal_batch).sum() > 0:
        print("#################################################")
    # if torch.cuda.is_available():
    #     next_state_goal_batch = next_state_goal_batch.cuda()
    #     reward_batch = reward_batch.cuda()
    #     action_batch = action_batch.cuda()
    #     state_goal_batch = state_goal_batch.cuda()
    next_target_actions = mu_target(next_state_goal_batch)
    if torch.isnan(next_target_actions).sum() > 0:
        print("*****************************************************************")

    q_target_input = torch.cat((next_state_goal_batch, next_target_actions), 1)
    q_target_input.to(device)
    a = q_target_input
    next_target_state_goal_action_values = Q_target(a).detach()
    expected_state_goal_action_values = reward_batch + (GAMMA * next_target_state_goal_action_values)
    expected_state_goal_action_values.clamp_(-1 / (1 - GAMMA), 0)
    q_input = torch.cat((state_goal_batch, action_batch), 1)
    # q_input.requires_grad = True
    Q_optimizer.zero_grad()
    state_goal_action_values = Q(q_input)

    #loss_critic = nn.MSELoss()
    loss = loss_critic(state_goal_action_values, expected_state_goal_action_values)
    # loss = ((state_goal_action_values - expected_state_goal_action_values) ** 2).sum() / len(state_goal_action_values)
    #loss = state_goal_action_values.mean()




    # Optimize the model
    loss.backward()
    for name, param in Q.named_parameters():
        param.grad.data.clamp_(-1, 1)

    Q_optimizer.step()

    mu_optimizer.zero_grad()
    action = mu(state_goal_batch)
    q_input = torch.cat((state_goal_batch, action), 1)
    # q_input.requires_grad = True
    state_values = - Q(q_input)
    loss = - state_values
    preactivation = mu.get_preacitvation(state_goal_batch)
    loss = loss.mean() + preactivation.sum()
    #    print("preacitvation {}".format(state_goal_batch))
    loss.backward()
    for name, param in mu.named_parameters():
        param.grad.data.clamp_(-1, 1)
    mu_optimizer.step()


######################################################################
# Main training loop
def plot_progress(rewads, n_epoc):
    if len(rewads) < 2:
        return
    sns.set(style="ticks", context="talk")
    plt.style.use("dark_background")
    y = np.array(rewads).reshape((1, -1))
    ax = sns.tsplot(data=y, ci="sd")
    mean = y.mean()
    std = y.std()
    # ax.errorbar([i for i in range(y.shape[1])], mean, yerr=std, fmt='-o')
    ax.set(xlabel='EPOCS', ylabel='# POSITIVE REWARDS')
    plt.savefig("score/progress.png")
    # with plt.style.context(('dark_background')):
    #     x = np.array([i for i in range(n_epoc+1)])
    #     y = np.array(rewads)
    #     plt.figure()
    #     plt.grid(True)
    #     plt.xlabel("EPOCS")
    #     plt.ylabel("# POSITIVE REWARDS")
    #     plt.plot(x, y, 'r')
    #     plt.savefig("score/progress.png")
    #     plt.close()


rewads_per_epoc = []
RENDER_STEPS = [i for i in range(0, 201, 20)]
RENDER_FLAG = False
frames = []
for i_epoc in range(MAX_EPOCS):
    # Will track each positive reward
    positive_rewards = 0
    RENDER_FLAG = True
    for i_cycle in range(MAX_CYCLES):

        for i_episode in range(MAX_EPISODES):
            # Initialize the environment and state
            observation = env.reset()
            state = observation[STATE]
            goal = observation[DESIRED_GOAL]
            state_goal = np.concatenate((state, goal))
            state_goal = torch.from_numpy(state_goal)
            state_action_pairs = []

            for t in range(MAX_TIME_STEPS):

                # Render the environment according to RENDER_RATE for only 1 episode
                # if RENDER_FLAG and (i_epoc in RENDER_STEPS):
                #     frames.append(Image.fromarray(env.render(mode='rgb_array')))

                # Select and perform an action
                action = select_action(state_goal)
                # if torch.cuda.is_available():
                #     action = action.cuda()
                observation, reward, done, info = env.step(action.cpu().numpy())

                if reward == 0:
                    positive_rewards += 1
                reward = torch.tensor([reward], device=device, dtype=torch.double)

                next_state = observation[STATE]
                achived_goal = observation[ACHIEVED_GOAL]
                next_state_goal = np.concatenate((next_state, goal))
                next_state_goal = torch.from_numpy(next_state_goal)

                # Store transition in memory
                replay_buffer.push(state_goal.cpu(), action.cpu(), next_state_goal.cpu(), reward.cpu(), info, t)
                state_action_pairs.append(State_Action(state_goal, action, next_state_goal, achived_goal, info, t))
                state_goal = next_state_goal

            for t in range(MAX_TIME_STEPS):
                time_step_transition = state_action_pairs[t].time_step
                goals = future(time_step_transition, state_action_pairs, NUM_GOALS)
                for goal in goals:
                    reward = env.compute_reward(state_action_pairs[t].achived_goal, goal, state_action_pairs[t].info)
                    reward = torch.tensor([reward], dtype=torch.double)
                    state_goal = state_action_pairs[t].state
                    action = state_action_pairs[t].action
                    next_state_goal = state_action_pairs[t].next_state
                    # Store future transition in memory
                    replay_buffer.push(state_goal.cpu(), action.cpu(), next_state_goal.cpu(), reward.cpu(),
                                       state_action_pairs[t].info,
                                       time_step_transition)

            # End time step loop

            # Save the environment rendering
            # if RENDER_FLAG:
            #     with open('score/FetchPush-v1__EPOC_' + str(i_epoc) + '__CYCLE_' + str(i_cycle) + '.gif', 'wb') as f:
            #         im = Image.new('RGB', frames[0].size)
            #         im.save(f, save_all=True, append_images=frames)
            #         RENDER_FLAG = False
            #         frames = []

        # End episode loop
        time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        print("Starting optimizing at {}".format(time))
        for i_optim_step in range(MAX_OPTIMIZATION_STEPS):
            optimize_model()
        time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        print("Optimization done at {}\tEpoc: {}\tCycle: {}".format(time, i_epoc, i_cycle))
        print("Optimization done at {}\tEpoc: {}\tCycle: {}".format(time, i_epoc, i_cycle),
              file=open('score/progress.txt', 'a'))
        # End optim loop
        Q, Q_target = updateTarget(Q, Q_target)
        mu, mu_target = updateTarget(mu, mu_target)
    # End cycle loop
    logg(mu, "logg_parameters/mu_parameters_" + str(i_epoc) + ".txt")
    logg(Q, "logg_parameters/Q_parameters_" + str(i_epoc) + ".txt")
    logg(mu_target, "logg_parameters/mu_target_parameters_" + str(i_epoc) + ".txt")
    logg(Q_target, "logg_parameters/Q_target_parameters_" + str(i_epoc) + ".txt")

    rewads_per_epoc.append(positive_rewards)
    plot_progress(rewads_per_epoc, i_epoc)
    torch.save(mu.state_dict(), 'model_trained/mu')
    torch.save(mu_target.state_dict(), 'model_trained/mu_target')
    torch.save(Q.state_dict(), 'model_trained/Q')
    torch.save(Q_target.state_dict(), 'model_trained/Q_target')
# End epoc loop
torch.save(mu.state_dict(), 'model_trained/mu_trained')
torch.save(mu_target.state_dict(), 'model_trained/mu_target_trained')
torch.save(Q.state_dict(), 'model_trained/Q_trained')
torch.save(Q_target.state_dict(), 'model_trained/Q_target_trained')
