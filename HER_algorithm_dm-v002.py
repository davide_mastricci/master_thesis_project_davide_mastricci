
# coding: utf-8

# ## HER in Pytorch (my implmentation) v.0.0.2

# In[ ]:


import torch.nn as nn
import torch.nn.functional as F
import torch
import gym
import numpy as np
import random
from collections import namedtuple
from time import gmtime, strftime
import pickle
import time

# Make code compatible with cuda
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

# Creating envirorment
env = gym.make('FetchReach-v1')


# In[ ]:


# These followinf string constant  are useful to extract the data from the observation returne by the envirorment like FetchReach
ACHIVED_GOAL = 'achieved_goal'
DESIRED_GOAL = 'desired_goal'
OBSERVATION = 'observation'
STATE = 'state'


# In[ ]:


# Training set-up specified in the paepr
MAX_EPOCS = 200
MAX_CYCLES = 50
MAX_EPISODES = 16
REPLAY_CAPACITY = 10e6
MINI_BATCH_SIZE = 128
MAX_TIME_STEPS = 50
MAX_STEPS_OPTIMIZATION = 40
EXPLORATION_PROBABILITY = 0.2 # e-greedy exploration
DECAY_COEFF = 0.95


# In[ ]:


# Goal limits specified in the paper
# First two coords represent the table (x,y), the last represent the height of the object (z)
GOAL_HIGHER_LIMIT = torch.Tensor([30, 30, 45]).to(device)
GOAL_LOWER_LIMIT = torch.Tensor([0, 0, 10]).to(device)


# In[ ]:


# Dimensions of the variable observable from the envirorment 
state_dim = env.observation_space.spaces[OBSERVATION].shape[0] # dim state of the env
goal_dim = env.observation_space.spaces[DESIRED_GOAL].shape[0] # dim goal - achived goal as the same space of desired since both are goals
action_dim = env.action_space.shape[0] # dim of the action


# In[ ]:


# dim of the hidden layer are shared in both Actor and Critic 
hidden_dim = 64


# In[ ]:


# DDPG classes (Actor-Critic)

class Actor(nn.Module):

    def __init__(self):
        super(Actor, self).__init__()
        
        # Retrive global variable from the python evn
        global GOAL_HIGHER_LIMIT
        global GOAL_LOWER_LIMIT
        global device


        # Network Architecture
        self.lin1 = nn.Linear(state_dim + goal_dim, hidden_dim)
        self.lin2 = nn.Linear(hidden_dim, hidden_dim)
        self.lin3 = nn.Linear(hidden_dim, action_dim)
        
        # Noise variable generation
        # For each coords of action space we got mean and standard deviation to compute the noise in the forward pass
        # This approach is stricktly relate to the action's dimension
        self.goal_higher_limit = GOAL_HIGHER_LIMIT
        self.goal_lower_limit = GOAL_LOWER_LIMIT
        self.points_x = torch.linspace(self.goal_lower_limit[0], self.goal_higher_limit[0]).to(device)
        self.mean_x = torch.mean(self.points_x).to(device)
        self.sd_x = self.points_x[4] # 5% of all possible value for x

        self.points_y = torch.linspace(self.goal_lower_limit[1], self.goal_higher_limit[1]).to(device)
        self.mean_y = torch.mean(self.points_y).to(device)
        self.sd_y = self.points_y[4] # 5% of all possible value for y
        
        self.points_z = torch.linspace(self.goal_lower_limit[2], self.goal_higher_limit[2]).to(device)
        self.mean_z = torch.mean(self.points_z).to(device)
        self.sd_z = self.points_z[4] # 5% of all possible value for z
        
        # preactivation will contain the valu of the input before the tanh
        self.preactivation = 0




        

    def forward(self, x):
        # Forward: ReLU -> ReLU -> Tanh -> Clamp
        
        x = F.relu(self.lin1(x))
        x = F.relu(self.lin2(x))
        
        # Getting tanh preactivation
        self.preactivation = x.clone()
        self.preactivation = self.preactivation.detach()
        
        x = F.tanh(self.lin3(x))
        x = torch.clamp(x, min=-5, max=5)
        
        # adding normal noise, with standard deviation equal to 5& of the total range of allowed values for each coordinare, to the output
        x[0] += torch.normal(self.mean_x, self.sd_x).to(device)
        x[1] += torch.normal(self.mean_y, self.sd_y).to(device)
        x[3] += torch.normal(self.mean_z, self.sd_z).to(device)
        
        return x
    
# End Actor class
    
class Critic(nn.Module):
    
    def __init__(self):
        super(Critic, self).__init__()
        # Network Architecture
        self.lin1 = nn.Linear(state_dim + action_dim + goal_dim, hidden_dim)
        self.lin2 = nn.Linear(hidden_dim, hidden_dim)
        self.lin3 = nn.Linear(hidden_dim, 1)
        
    def forward(self, x):
        # Forward: ReLU -> ReLU -> ReLU
        x = F.relu(self.lin1(x))
        x = F.relu(self.lin2(x))
        x = F.relu(self.lin3(x))
        return x
    
# End Critic class


# In[ ]:


# Format of the transition wich will be stored in ReplayMemory
Transition = namedtuple('Transition',
                        ('state', 'action', 'reward', 'next_state', 'goal', 'episode', 'done'))


# In[ ]:


# Replay memory classes

class ReplayMemory(object):

    def __init__(self, capacity):
        """ Create an instance of replay memory.
            
            Args:
                capacity (int): the capacity of the replay memory. When the the replay memory is full it starts overwriting the elements."""
        self.capacity = capacity
        self.memory = []
        self.position = 0
        self.last_element = 0

    def push(self, *args):
        """ Saves a transition.
            
            Args:
                *args : state, action, reward, next_state, goal, episode, done"""
        if len(self.memory) < self.capacity:
            self.memory.append(None)
        self.memory[self.position] = Transition(*args)
        self.last_element = self.position
        self.position = int((self.position + 1) % self.capacity)

    def sample(self, batch_size): 
        """Sample a batch of transition form the replay mamory.
        
            Args:
                batch_size (int): number of element to sample"""
        state_batch, action_batch, reward_batch, next_state_batch,  goal_batch, episode_batch, done_mask = zip(*random.sample(self.memory, batch_size))

        state_batch = np.array(state_batch)
        action_batch = np.array(action_batch)
        reward_batch = np.array(reward_batch)
        next_state_batch = np.array(next_state_batch)
        done_mask = np.array(done_mask)
        goal_batch = np.array(goal_batch)
        episode_batch = np.array(episode_batch)

        return state_batch, action_batch, reward_batch, next_state_batch, goal_batch, episode_batch, done_mask
    
    # Not Used
    def get_last_transaction(self):
        state, action, reward, next_state, goal, episode, done = self.memory[self.last_element]
        return state, action, reward, next_state, goal, episode, done
         

    def __len__(self):
        return len(self.memory)
    
# End ReplayMemory class


# In[ ]:


# Utilis function to set and update paramenters of actor-critic network
# Credits: https://github.com/ikostrikov/pytorch-ddpg-naf/blob/master/ddpg.py

# Function to ensure that target (stable network) paramsare the same of training natwork params
def setTarget(train, target):
    """ Make sure target params are the same of training network
    
        Args:
            train (torch.nn.Module): training network
            target (torch.nn.Module): target network
            
        Returns:
            train (torch.nn.Module): training network
            target (torch.nn.Module): target network


    """
    
    for target_param, param in zip(target.parameters(), train.parameters()):
            target_param.data.copy_(param.data)
            
    return train, target
            
            
def updateTarget(train, target, decay_coeff=0.95):
    """Update Target netwotk with training network parameters
    
        Args:
            train (torch.nn.Module): training network
            target (torch.nn.Module): target network
            decay_coeff (float64): decay coefficent applied to the target network
        
        Returns:
            train (torch.nn.Module): training network
            target (torch.nn.Module): target network
    """

    for target_param, param in zip(target.parameters(), train.parameters()):
        target_param.data.copy_(target_param.data * (1.0 - decay_coeff) + param.data * decay_coeff)
        
    return train, target


# In[ ]:


# Utilis for scaling vector

def scaleInput(setOfObservation, inputVector, min=-5, max=5):
    """ Scale the input vector so its elments will have zero mean a standard deviation equal to one and then they are clipped to min max range.
        
        Args:
            setOfObservation (torch.Tensor): observation used to compute mean and standard deviation
            inputVector (torch.Tensor): input to scale
            
        Returns:
            (torch.Tensor): InputVector scaled"""
                    
    mean = setOfObservation.mean()
    standard_deviation = setOfObservation.std()
    inputVector = inputVector.cpu() - mean.cpu()
    inputVector = inputVector.cpu() / standard_deviation.cpu()              
    inputVector = torch.clamp(inputVector,min,max).to(device)
    return inputVector


# In[ ]:


# Networks generation        
actor = Actor().to(device)
critic = Critic().to(device)
actor_stable = Actor().to(device)
critic_stable = Critic().to(device)

# Networks initialization       
actor, actor_stable = setTarget(actor, actor_stable)
critic, critic_stable = setTarget(critic, critic_stable)


# In[ ]:


# Optimizer generation
lr_adam = 0.001 # spcified in the paper
critic_optim = torch.optim.Adam(critic.parameters(), lr=lr_adam)
actor_optim = torch.optim.Adam(actor.parameters(), lr=lr_adam)


# In[ ]:


# Replay memory generation
replay_buffer = ReplayMemory(REPLAY_CAPACITY)


# In[ ]:


#Training parameter
gamma = 0.98 #discount factor Bellman equation


# In[ ]:


# Useful to store tempory transition without rewards which will computed in HER replay

ReplayTransition = namedtuple('ReplayTransition',
                        ('state', 'action', 'achived_goal','next_state', 'desired_goal', 'episode', 'done', 'info')) 


# ## Training Loop

# In[ ]:


list_of_replay_transitions = []

loss_critic = nn.MSELoss()

set_of_input_actor = [] # useful to track the observation used during the training so we can rescal the input

set_of_input_critic = [] # same as above



success_rate = []

for epoc in range(MAX_EPOCS): 
    
    print("---------- Epoc: {} --------------------".format(epoc), file=open("score/epocs_done.txt", "w"))
    print("Starting epoc {} of {}".format(epoc, MAX_EPOCS), end="\t")
    print(strftime("%Y-%m-%d %H:%M:%S", gmtime()))
    
    # Useful to take track of the positive rewards
    rewards = 0
    
    for cycle in range(MAX_CYCLES):
        print("\tStarting cycle {} of {}".format(cycle, MAX_CYCLES))

        for episode in range(MAX_EPISODES):
            print("\t \t Starting episode {} of {}".format(episode, MAX_EPISODES))

            # Resetting the envirorment
            observation = env.reset()
            
            # Taking the initial state and the initial goal
            goal = torch.Tensor(observation[DESIRED_GOAL]).to(device)
            state = torch.Tensor(observation[OBSERVATION]).to(device)
            


            for time_step in range(MAX_TIME_STEPS):

                # Concatenating state with goal as specified in HER paper
                input_actor = torch.cat((state.cpu(), goal.cpu())).to(device)
                
                # Scaling the input
                set_of_input_actor.append(input_actor)
                input_actor = scaleInput(torch.stack(set_of_input_actor,dim=0).to(device), input_actor).to(device)
                
                
                # e-greedy strategy
                if random.random() > EXPLORATION_PROBABILITY:
                    action = actor.forward(input_actor)
                    # OpenAI envirorment use only numpy array so it is necessary to detach the node e make the cast to a numpy array
                    action = action.cpu()
                    action = action.detach().numpy()
                else: 
                    action = env.action_space.sample()
                    
                
                # Taking the action according to e-greedy strategy 
                next_observation, reward, info, done = env.step(action)


                # Observing the new state and the goal achived
                achived_goal = next_observation[ACHIVED_GOAL]   
                next_state = next_observation[OBSERVATION]
                
                
                list_of_replay_transitions.append(ReplayTransition(state.cpu().detach().numpy(), action, achived_goal, next_state, goal.cpu().detach().numpy(), episode, done, info))

                # Now the nex_state become the current state for the next iteration
                state = torch.Tensor(next_state)
                
                # Traking good performance (0 = positive reward) (-1 negative reward)
                if reward == 0:
                    rewards +=1
                    
                    
            # End time step loop
            
            # Useful to achive final HER strategy
            last_state_of_episode = achived_goal   



            # HER replay
            for transition in list_of_replay_transitions:
                # Standard replay
                reward = env.compute_reward(transition.achived_goal, transition.desired_goal, transition.info)
                replay_buffer.push(transition.state, transition.action, reward, transition.next_state, transition.desired_goal, transition.episode, transition.done)
                # HER replay final
                reward = env.compute_reward(transition.achived_goal, last_state_of_episode, transition.info)
                replay_buffer.push(transition.state, transition.action, reward, transition.next_state, last_state_of_episode, transition.episode, transition.done) # HER replay
                
                
            # end HER replay 
            
        # End episode loop

        for i in range(MAX_STEPS_OPTIMIZATION):
            print("\t \t \t \t Starting optim_step {} of {}".format(i, MAX_STEPS_OPTIMIZATION))

            # Sampling batch
            state_batch, action_batch, reward_batch, next_state_batch, goal_batch, episode_batch, done_mask =  replay_buffer.sample(MINI_BATCH_SIZE)

            y_input = []
            y_target = []

            for sample_index in range(MINI_BATCH_SIZE):
                # here we are going to update the networks using MSE for critic and gradient update for actor
                # we need to compute y_target (prediction using Bellman eq.) and y_input using the output 
                # of critic

                # To compute y_input we need reward and the q_value of next_observation + goal and next_action,
                # the last is taken using actor network

                # Compute next_observation || goal
                next_observation = torch.Tensor(next_state_batch[sample_index]).to(device)
                goal = torch.Tensor(goal_batch[sample_index])
                next_state = torch.cat((next_observation.cpu(), goal.cpu())).to(device)

                # Compute next_action
                next_action = actor_stable.forward(next_state)

                # Compute y_input using reward, and q_value(next_observation || goal, next_action)
                input_critic = torch.cat((next_state.cpu(), next_action.cpu()))
                # Scale the input 
                set_of_input_critic.append(input_critic)
                input_critic = scaleInput(torch.stack(set_of_input_critic, dim=0).to(device), input_critic).to(device)

                # float cast is needed because numpy.float32 create problem with torch.Tensor
                y_target_i = float(reward_batch[sample_index]) + gamma * critic_stable.forward(input_critic.to(device))


                # Compute y_target using q_value(current_state || goal, action)
                current_state = torch.Tensor(state_batch[sample_index]).to(device)
                action = torch.Tensor(action_batch[sample_index]).to(device)
                input_critic = torch.cat((current_state.cpu(), goal.cpu()))
                input_critic = torch.cat((input_critic.cpu(), action.cpu()))

                # Scale input
                input_critic = scaleInput(torch.stack(set_of_input_critic, dim=0).to(device), input_critic).to(device)
                y_input_i = critic.forward(input_critic.to(device))

                # Append y_input_i and y_target_i to a matrix with all y_inputs and y_target useful
                # to compute the MSE loss on critic network
                y_target.append(y_target_i)
                y_input.append(y_input_i)



            # Set gradient to zero to avoid cumulating gradient 
            critic_optim.zero_grad()
            

            y_input = torch.stack(y_input, dim=0).to(device)
            y_target = torch.stack(y_target, dim=0).to(device)
            y_target = torch.clamp(y_target.cpu(), min = -(1 / (1-gamma)), max = 0).to(device)
            y_target = y_target.detach()

            # Default behavior of Pytorch 0.4 does not mantain gradient. Setting this to True we force to manatain the gradient on y_input
            # which is the output of the graph critic network
            #y_input.requires_grad = True

            # Critic update
            loss = loss_critic(y_input, y_target)
            loss.backward(retain_graph=True)
            critic_optim.step()
            y_input.detach()
            
            # Set gradient to zero to avoid cumulating gradient
            actor_optim.zero_grad()

            # Actor update using the mean of gradient of critic
            state_batch = torch.Tensor(state_batch).to(device)
            goal_batch = torch.Tensor(goal_batch).to(device)
            action_batch = torch.Tensor(action_batch).to(device)
            loss_actor_input = torch.cat((state_batch.cpu(),goal_batch.cpu(), action_batch.cpu()), dim=1).to(device)
            critic_input = scaleInput(torch.stack(set_of_input_critic, dim=0).to(device), loss_actor_input).to(device)
            critic_output = critic.forward(critic_input).detach()
            loss_actor = - torch.mean(critic_output * actor(torch.cat((state_batch, goal_batch), dim=1).to(device)))
            loss_actor += (actor.preactivation**2).sum()
            loss_actor.backward(retain_graph=True)
            actor_optim.step()
            



        # End optimization loop
        
        # Target (Stable) networks update for stabilization
        # The paper specifiea to update the nerworks after every cycle
        critic, critic_stable = updateTarget(critic, critic_stable, DECAY_COEFF)
        actor, actor_stable = updateTarget(actor, actor_stable, DECAY_COEFF)

    print("Cycle {} of epoc {} completed".fomrat(cycle, epoc))
    # end cycles loop
    succes = rewards / (MAX_CYCLES * MAX_EPISODES * MAX_TIME_STEPS)
    print("Episode: {} \nSuccess rate: {}".format(epoc, succes), file=open("score/success_rate.txt", "a"))
    print("Episode: {} \nSuccess rate: {}".format(epoc, succes))
    
    success_rate.append(succes)

# end epocs loop


# In[ ]:


torch.save(actor.state_dict(), 'model_trained/actor')
torch.save(actor_stable.state_dict(), 'model_trained/actor_stable')
torch.save(critic.state_dict(), 'model_trained/critic')
torch.save(critic_stable.state_dict(), 'model_trained/critic_stable')


# In[ ]:


with open('model_trained/success_rate.pkl', 'wb') as f:
    pickle.dump(success_rate, f)

