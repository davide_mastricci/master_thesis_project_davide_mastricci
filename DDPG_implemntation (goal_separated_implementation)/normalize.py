import numpy as np
import pickle


class Normalizer():
    def __init__(self, size):
        """

        :param size: observation dimensions
        """
        self.n = np.zeros(size)
        self.mean = np.zeros(size)
        self.mean_diff = np.zeros(size)
        self.var = np.zeros(size)

    def observe_normalize(self, x, clip=5):
        """
        Add a point and then compute the normalization.
        :param x: observation to be normalized
        :param clip: normalized observations are clipped to be in [-clip, clip]
        :return: observation normalized
        """
        self.n += 1.
        last_mean = self.mean.copy()
        self.mean += (x - self.mean) / self.n
        self.mean_diff += (x - last_mean) * (x - self.mean)
        self.var = np.clip(self.mean_diff / self.n, a_min=1e-2, a_max=np.inf)
        obs_std = np.sqrt(self.var)
        x = np.clip((x - self.mean) / obs_std, a_min=-clip, a_max=clip)
        return x

    def normalize(self, x, clip=5):
        """Compute the normalization"""
        last_mean = self.mean.copy()
        self.mean += (x - self.mean) / self.n
        self.mean_diff += (x - last_mean) * (x - self.mean)
        self.var = np.clip(self.mean_diff / self.n, a_min=1e-2, a_max=np.inf)
        obs_std = np.sqrt(self.var)
        x = np.clip((x - self.mean) / obs_std, a_min=-clip, a_max=clip)
        return x

        self.var = np.clip(self.mean_diff / self.n, a_min=1e-2, a_max=np.inf)
        obs_std = np.sqrt(self.var)
        x = np.clip((x - self.mean) / obs_std, a_min=-clip, a_max=clip)
        return x

    def serialize(self, path):
        data = {"n": self.n.tolist(), "mean": self.mean.tolist(), "mean_diff": self.mean_diff.tolist(), "var": self.var.tolist()}
        outfile = open(path, "wb")
        pickle.dump(data,outfile)
        outfile.close()

    def deserialize(self, path):
        infile = open(path, "rb")
        data = pickle.load(infile)
        infile.close()
        self.n = np.array(data['n'])
        self.mean = np.array(data['mean'])
        self.mean_diff = np.array(data['mean_diff'])
        self.var = np.array(data['var'])
