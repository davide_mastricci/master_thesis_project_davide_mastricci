# RL Project tracks
1. [Robotics ideas](#robotics)

2. [Navigation ideas](#navigation)

## Robotics ideas
<a name='robotics'> </a>
### Note Envirorment
Quello che segue deriva da un esperinza diretta con l'ambiente [FetchPickAndPlace-v1](https://gym.openai.com/envs/FetchPickAndPlace-v0/)(il v0 è deprecato) più le informazioni trovate in [questo paper](https://arxiv.org/pdf/1802.09464.pdf)

 Gli ambienti che hanno il braccio meccanico non si riferiscono a prototipi presenti nell'azienda Shadow Robotic ma bensì a [Fetch Robotics](https://fetchrobotics.com). Il robot su cui si basano è il 7-DoF robotics arm prodotto dalla stessa azienda.

 Glia amienti hanno le seguenti caratteristiche:

 - Il **goal** é descritto come un vettore **3-D** che indica la posizione finale dell'oggetto, ovvero dove si deve trovare l'oggetto preso dal braccio meccanico a fine task (marker).
 - I **reward** sono *sparsi* e *binari* (ma esistono anche le versioni degli stessi ambienti con reward densi).
  - L'agente ottine **0** se l'oggetto è nella posizione desiderata (marker), con una tollleranza di 5 cm, e **-1** altrimenti.

- Le **azioni** sono descritte da un vettore **4-D**:
 - Le prime 3 dimensioni indicano il movimeto che il braccio deve fare (coordinate x,y,z) sugli assi cartesiani. L'ultima dimensione indica l'apertuta o la chiusura del gripper attaccato al braccio.
 - Il valore massimo che un'azione può assumere è **[1, 1, 1]**, invece il valore minimo è **[-1, -1, -1]**
 - Dato che l'ambiente è dipendete dal motore fisico MujoCo, presa un azione a, questa viene applicata in 20 step del simulatore.

- Le **osservazioni** sono descritte da 3 vettori:
 - Il primo vettore ha 3 dimensioni e indica l'**achived goal**
 - Il secondo vetttore ha 3 dimenioni e indica il **desired goal**
 - 3-D, posizione del gripper
 - 1-D velocità lineare del gripper
 - 3-D posizione del braccio meccanico
 - 1-D velocità lineare del braccii
 - 3-D posizione dell'oggetto
 - 3-D la rotazione dell'oggetto usando gli [angoli di Eulero](https://it.wikipedia.org/wiki/Angoli_di_Eulero)
 - 1-D la velocità lineare dell'oggetto
 - 3-D la velocità angolare dell'oggetto
 - 3-D posizione dell'oggetto relativa al gripper
 - 1-D velicità lineare dell'oggetto relativa al gripper
 - 3-D velocità angolare dell'oggetto relativa al gripper
 - I valore massimi che si possono esservare per le osservazini sono [Inf,..., Inf], mentre i valori minimi sono [-Inf,...,-Inf]


### Note Hindsight Experience Replay (HER)

OpenAI ha pubblicato il codice per la replica dei risultati ottenuti nel paper ma tale codice é possibile applicarlo solo al task del [reaching](https://gym.openai.com/envs/FetchReach-v0/) dove però l'ambiete originale (a.k.a FetchReach-v0) è stato sostituo con un suo update (FetchReach-v1).


### Paper di Riferiemento per il problema dello sparse Reward
- **Hindsight Experience Replay** : viene risolto il problema dei reward sparsi e binari, può essere applicato con qualunque algoritmo off policy.
- **Multi-Goal Reinforcement Learning: Challeinging Robotics Envirorments and Request for Research**: é il report tecnico che descrive gli ambienti robotici rilasciati da OpenAI e contiene alcui suggeriemnti per la ricerca.
- **Leveraging Demonstrations for Deep Reinforcement Learning on Robotics Problems with Sparse Rewards** : [IN LETTURA]
- **Learning by Playing – Solving Sparse Reward Tasks from Scratch** : [DA LEGGERE]


##Navigation ideas

<a name='navigation'></a>

Mettere insieme i risultati ottenuti dal pepr MapNet con un algoritmo di DRL
