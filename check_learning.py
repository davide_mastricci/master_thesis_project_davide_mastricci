import torch
import torch.nn as nn
import torch.nn.functional as F
import numpy as np
import gym


env = gym.make("FetchPush-v1")

# These following string constant are useful to extract the data from the observation returned by the envirorment
ACHIEVED_GOAL = 'achieved_goal'
DESIRED_GOAL = 'desired_goal'
STATE = 'observation'


# DIMENSION OF THE ENVIRONMENT
# In the general approach of HER the state is combined with the goal
STATE_GOAL_DIM = env.observation_space.spaces[STATE].shape[0] + env.observation_space.spaces[DESIRED_GOAL].shape[0]
STATE_DIM = env.observation_space.spaces[STATE].shape[0]
GOAL_DIM = env.observation_space.spaces[DESIRED_GOAL].shape[0]
ACTION_DIM = env.action_space.shape[0]


class Actor(nn.Module):

    def __init__(self):
        super(Actor, self).__init__()
        # Network architecture
        # self.HIDDEN_DIM = 64  # paper values
        self.HIDDEN_DIM = 256  # experiment values
        self.lin1 = nn.Linear(STATE_GOAL_DIM, self.HIDDEN_DIM)
        self.lin2 = nn.Linear(self.HIDDEN_DIM, self.HIDDEN_DIM)
        self.lin3 = nn.Linear(self.HIDDEN_DIM, self.HIDDEN_DIM)
        self.lin4 = nn.Linear(self.HIDDEN_DIM, ACTION_DIM)
        torch.nn.init.xavier_uniform_(self.lin1.weight)
        torch.nn.init.xavier_uniform_(self.lin2.weight)
        torch.nn.init.xavier_uniform_(self.lin3.weight)
        torch.nn.init.xavier_uniform_(self.lin4.weight)
        self.preactivation = None

    def forward(self, x):
        # x = torch.clamp(x, min=-5, max=5)
        x = F.relu(self.lin1(x))
        x = F.relu(self.lin2(x))
        x = F.relu(self.lin3(x))
        self.preactivation = torch.tensor(x.data, dtype=torch.double)
        x = F.tanh(self.lin4(x))
        # x = x * max_u
        # x = torch.clamp(x, min=-0.05, max=0.05)

        return x

    def get_preacitvation(self, x):
        """Get the activiation before the tanh pass.

            Args:
                x (torch.Tensor): input for the network"""

        return self.preactivation


mu = Actor()
mu.load_state_dict(torch.load("model_trained/mu_target"))
mu.double()

for i_episode in range(5):
    # Initialize the environment and state
    observation = env.reset()
    env.render()
    state = observation[STATE]

    goal = observation[DESIRED_GOAL]
    state_goal = np.concatenate((state, goal))
    state_goal = torch.from_numpy(state_goal)
    state_action_pairs = []

    for t in range(20):
        action = mu(state_goal)

        observation, reward, done, info = env.step(action.detach().cpu().numpy())
        print(reward)
        env.render()
