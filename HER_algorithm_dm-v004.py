import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch
import gym
import numpy as np
from collections import namedtuple
import random
import matplotlib.pyplot as plt
from time import gmtime, strftime
import seaborn as sns
from PIL import Image
import scipy.spatial.distance as dist

# import pdb

##########################################################################
# In this fourth version I'm going to add an new extra layer into both
# Actor and Critic Network to test my possible misunderstanding of
# the architecture

# Creating environment
env = gym.make('FetchPush-v1')

# Making code compatible with cuda GPU
# device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
device = torch.device("cpu")

# These following string constant are useful to extract the data from the observation returned by the envirorment
ACHIEVED_GOAL = 'achieved_goal'
DESIRED_GOAL = 'desired_goal'
STATE = 'observation'

# DIMENSION OF THE ENVIRONMENT
# In the general approach of HER the state is combined with the goal
STATE_GOAL_DIM = env.observation_space.spaces[STATE].shape[0] + env.observation_space.spaces[DESIRED_GOAL].shape[0]
STATE_DIM = env.observation_space.spaces[STATE].shape[0]
GOAL_DIM = env.observation_space.spaces[DESIRED_GOAL].shape[0]
ACTION_DIM = env.action_space.shape[0]

#################################################################################################################
# Replay Memory
# -------------
#
# We'll be using experience replay memory for training our DDPG. It stores
# the transitions that the agent observes , allowing us to reuse this data
# later. By sampling from it randomly, the transitions that build up a
# batch are decorrelated.
#
# For this, we're going to need two classes:
#
# -  ``Transition`` - a named tuple representing a single transition in
#    our environment
# -  ``ReplayMemory`` - a cyclic buffer of bounded size that holds the
#    transitions observed recently. It also implements a ``.sample()``
#    method for selecting a random batch of transitions for training.
#
# The following approach is inspired by https://pytorch.org/tutorials/intermediate/reinforcement_q_learning.html
################################################################################################################

Transition = namedtuple('Transition',
                        ('state_goal', 'action', 'next_state_goal', 'reward', 'info', 'time_step'))


class ReplayMemory(object):

    def __init__(self, capacity):
        self.capacity = capacity
        self.memory = []
        self.position = 0

    def push(self, *args):
        """ Saves a transition.

            Args:
                *args : state_goal, action, reward, next_state_goal, episode, done"""
        if len(self.memory) < self.capacity:
            self.memory.append(None)
        self.memory[self.position] = Transition(*args)
        self.position = int((self.position + 1) % self.capacity)

    def sample(self, batch_size):
        """Sample a batch of transition form the replay mamory.

            Args:
                batch_size (int): number of element to sample

            Returns: ...
                """

        return random.sample(self.memory, batch_size)

    def __len__(self):
        return len(self.memory)


######################################################################
# DDPG Model (Actor-Critic) classes
# -------------
######################################################################


class Critic(nn.Module):

    def __init__(self):
        super(Critic, self).__init__()
        # Network architecture
        # self.HIDDEN_DIM = 64  # paper values
        self.HIDDEN_DIM = 256  # experiment value https://github.com/openai/baselines/blob/master/baselines/her/experiment/config.py
        self.lin1 = nn.Linear(STATE_GOAL_DIM + ACTION_DIM, self.HIDDEN_DIM)
        self.lin2 = nn.Linear(self.HIDDEN_DIM, self.HIDDEN_DIM)
        self.lin3 = nn.Linear(self.HIDDEN_DIM, self.HIDDEN_DIM)
        self.lin4 = nn.Linear(self.HIDDEN_DIM, 1)
        torch.nn.init.xavier_uniform_(self.lin1.weight)
        torch.nn.init.xavier_uniform_(self.lin2.weight)
        torch.nn.init.xavier_uniform_(self.lin3.weight)
        torch.nn.init.xavier_uniform_(self.lin4.weight)

    def forward(self, x):
        # x = torch.clamp(x, min=-5, max=5)
        x = F.relu(self.lin1(x))
        x = F.relu(self.lin2(x))
        x = F.relu(self.lin3(x))
        x = self.lin4(x)
        return x


class Actor(nn.Module):

    def __init__(self):
        super(Actor, self).__init__()
        # Network architecture
        # self.HIDDEN_DIM = 64  # paper values
        self.HIDDEN_DIM = 256  # experiment values
        self.lin1 = nn.Linear(STATE_GOAL_DIM, self.HIDDEN_DIM)
        self.lin2 = nn.Linear(self.HIDDEN_DIM, self.HIDDEN_DIM)
        self.lin3 = nn.Linear(self.HIDDEN_DIM, self.HIDDEN_DIM)
        self.lin4 = nn.Linear(self.HIDDEN_DIM, ACTION_DIM)
        torch.nn.init.xavier_uniform_(self.lin1.weight)
        torch.nn.init.xavier_uniform_(self.lin2.weight)
        torch.nn.init.xavier_uniform_(self.lin3.weight)
        torch.nn.init.xavier_uniform_(self.lin4.weight)
        self.preactivation = None

    def forward(self, x):
        # x = torch.clamp(x, min=-5, max=5)
        x = F.relu(self.lin1(x))
        x = F.relu(self.lin2(x))
        x = F.relu(self.lin3(x))
        self.preactivation = torch.tensor(x.data, dtype=torch.double)
        x = F.tanh(self.lin4(x))
        # x = x * max_u
        # x = torch.clamp(x, min=-0.05, max=0.05)

        return x

    def get_preacitvation(self, x):
        """Get the activiation before the tanh pass.

            Args:
                x (torch.Tensor): input for the network"""

        return self.preactivation


env.reset()

############################################################################################################
# Training
# --------
#
# Hyperparameters and utilities
# ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
# The following section instantiates our networks and their optimizer, and defines some
# utilities:
#
# -  ``select_action`` - will select an action accordingly to an epsilon
#    greedy policy.The probability of choosing a random action will be at
#    ``EXPLORATION_PROBABILITY`` .
#
# -  ``updateTarget`` - will update target (stable) network with training networks's weights.
#
# -  ``future`` - will extract random states (``num_states``) from the same episode as the transition begin
#      replayed and were observed after it.
############################################################################################################

BATCH_SIZE = 256  # https://github.com/openai/baselines/blob/master/baselines/her/experiment/config.py
MAX_EPOCS = 200
MAX_CYCLES = 50
MAX_EPISODES = 16
MAX_TIME_STEPS = 50
MAX_OPTIMIZATION_STEPS = 40
EXPLORATION_PROBABILITY = 0.3
# Decay coefficient used to update target networks' weights during the training
DECAY_COEFF = 0.95
LR_ADAM = 0.001
REPLAY_BUFFER_CAPACITY = 10e6
# Discount factor for Bellman equation used in critic (Q) loss function
GAMMA = 0.98
# NUmber of virtual goal to replay
NUM_GOALS = 4


def select_action(state):
    global mu

    with torch.no_grad():
        GOAL_HIGHER_LIMIT = 1
        GOAL_LOWER_LIMIT = -1

        points = torch.linspace(GOAL_LOWER_LIMIT, GOAL_HIGHER_LIMIT)
        mean = torch.mean(points)
        std = points[1]  # 5% of all possible value for x
        if random.random() > EXPLORATION_PROBABILITY:
            # if torch.cuda.is_available():
            #     state = state.cuda()
            a = mu(state)
            # adding noise to each coords
            a += torch.normal(mean, std).double()
            return a
        else:
            return torch.tensor(env.action_space.sample(), dtype=torch.double)


def updateTarget(train, target, decay_coeff=0.95):
    """Update Target netwotk with training network parameters

        Args:
            train (torch.nn.Module): training network
            target (torch.nn.Module): target network
            decay_coeff (float64): decay coefficent applied to the target network

        Returns:
            train (torch.nn.Module): training network
            target (torch.nn.Module): target network
    """

    for target_param, param in zip(target.parameters(), train.parameters()):
        target_param.data.copy_(target_param.data * (1.0 - decay_coeff) + param.data * decay_coeff)

    return train, target


State_Action = namedtuple('State_Action', ('state', 'action', 'next_state', 'achived_goal', 'info', 'time_step'))


def future(time_step, state_action_pairs, num_goals=NUM_GOALS):
    future_states = []
    while len(future_states) < num_goals:
        for i in range(time_step + 1, MAX_TIME_STEPS + 1):
            future_states.append(state_action_pairs[t].achived_goal)

    # if len(future_states) <= num_goals:
    #     return future_states
    # else:
    return random.choices(future_states, k=num_goals)


class Normalizer():
    def __init__(self, size):
        """

        :param size: observation dimensions
        """
        self.n = np.zeros(size)
        self.mean = np.zeros(size)
        self.mean_diff = np.zeros(size)
        self.var = np.zeros(size)

    def normalize(self, x, clip=5):
        """

        :param x: observation to be normalized
        :param clip: normalized observations are clipped to be in [-clip, clip]
        :return: observation normalized
        """
        self.n += 1.
        last_mean = self.mean.copy()
        self.mean += (x - self.mean) / self.n
        self.mean_diff += (x - last_mean) * (x - self.mean)
        self.var = np.clip(self.mean_diff / self.n, a_min=1e-2, a_max=np.inf)
        obs_std = np.sqrt(self.var)
        x = np.clip((x - self.mean) / obs_std, a_min=-clip, a_max=clip)
        return x


mu = Actor().to(device)
mu_target = Actor().to(device)
mu_target.load_state_dict(mu.state_dict())
mu_target.eval()
mu.train()
Q = Critic().to(device)
Q_pi = Critic().to(device)
Q_target = Critic().to(device)
Q_target.load_state_dict(Q.state_dict())
Q_pi.load_state_dict(Q.state_dict())
Q_target.eval()
Q.train()

mu.double()
mu_target.double()
Q.double()
Q_target.double()

mu_optimizer = optim.Adam(mu.parameters())  # Default parameter of lr is already 0.001
Q_optimizer = optim.Adam(Q.parameters())

loss_critic = torch.nn.MSELoss()
replay_buffer = ReplayMemory(REPLAY_BUFFER_CAPACITY)


# mu_normalizer = Normalizer()
# Q_normalizer = Normalizer()


def logg(model, path):
    file = open(path, "w")
    file.close()
    for name, param in model.named_parameters():
        if param.requires_grad:
            print("{}\t{}".format(name, str(param.data)), file=open(path, "a"))


######################################################################
# Training loop
# ^^^^^^^^^^^^^
#

loss_critic_list = []


def optimize_model():
    global mu
    global mu_target
    global mu_optimizer
    global Q
    global Q_pi
    global Q_target
    global Q_optimizer
    global replay_buffer
    global loss_critic
    global loss_critic_list

    if len(replay_buffer) < BATCH_SIZE:
        return
    transitions = replay_buffer.sample(BATCH_SIZE)
    batch = Transition(*zip(*transitions))

    state_goal_batch = torch.stack(batch.state_goal)
    action_batch = torch.stack(batch.action)
    reward_batch = torch.stack(batch.reward)
    #    print("Percentage of zero = {} %".format((128 - torch.nonzero(reward_batch).size(0)) / 128 * 100))
    next_state_goal_batch = torch.stack(batch.next_state_goal)
    if torch.isnan(state_goal_batch).sum() > 0 or torch.isnan(action_batch).sum() > 0 or torch.isnan(
            reward_batch).sum() > 0 or torch.isnan(next_state_goal_batch).sum() > 0:
        print("#################################################")


    #########################################################################
    # CRITIC TRAINING
    # ---------------
    #########################################################################

    # if torch.cuda.is_available():
    #     next_state_goal_batch = next_state_goal_batch.cuda()
    #     reward_batch = reward_batch.cuda()
    #     action_batch = action_batch.cuda()
    #     state_goal_batch = state_goal_batch.cuda()

    next_target_actions = mu_target(next_state_goal_batch)
    if torch.isnan(next_target_actions).sum() > 0:
        print("*****************************************************************")

    q_target_input = torch.cat((next_state_goal_batch, next_target_actions), 1)
    q_target_input.to(device)
    a = q_target_input
    next_target_state_goal_action_values = Q_target(a).detach()
    expected_state_goal_action_values = reward_batch + (GAMMA * next_target_state_goal_action_values)
    expected_state_goal_action_values.clamp_(-1 / (1 - GAMMA), 0)
    q_input = torch.cat((state_goal_batch, action_batch), 1)
    # q_input.requires_grad = True
    Q_optimizer.zero_grad()
    state_goal_action_values = Q(q_input)

    # loss_critic = nn.MSELoss()
    loss = loss_critic(state_goal_action_values, expected_state_goal_action_values)
    # loss = ((state_goal_action_values - expected_state_goal_action_values) ** 2).sum() / len(state_goal_action_values)
    # loss = state_goal_action_values.mean()

    # Optimize the model
    loss.backward()
    torch.nn.utils.clip_grad_norm_(Q.parameters(), 1)
    print("{}".format(loss), file=open("score/critic_losses.log", 'a'))
    # for name, param in Q.named_parameters():
    #     param.grad.data.clamp_(-1, 1)

    Q_optimizer.step()
    # loss_critic_list.append(loss.detach().numpy())

    ################################################################################
    # POLICY TRAINING
    # ---------------
    #################################################################################
    mu_optimizer.zero_grad()
    action = mu(state_goal_batch)
    q_input = torch.cat((state_goal_batch, action), 1)
    # q_input.requires_grad = True
    state_values = Q(q_input)
    loss = state_values
    # preactivation = (mu.get_preacitvation(state_goal_batch) ** 2).sum()
    mean = - loss.mean()
    loss = mean + (action ** 2).mean()
    loss.backward()
    torch.nn.utils.clip_grad_norm_(mu.parameters(), 1)
    print("{}".format(loss), file=open("score/actor_losses.log", 'a'))




def plot_progress(rewads):
    if len(rewads) < 2:
        return
    plt.clf()
    sns.set(style="ticks", context="talk")
    plt.style.use("dark_background")
    y = np.array(rewads).reshape((1, -1))
    ax = sns.tsplot(data=y, ci="sd")
    mean = y.mean()
    std = y.std()
    # ax.errorbar([i for i in range(y.shape[1])], mean, yerr=std, fmt='-o')
    ax.set(xlabel='EPOCS', ylabel='% SUCCESS RATE')
    plt.savefig("score/progress.png")
    plt.close()
    # with plt.style.context(('dark_background')):
    #     x = np.array([i for i in range(n_epoc+1)])
    #     y = np.array(rewads)
    #     plt.figure()
    #     plt.grid(True)
    #     plt.xlabel("EPOCS")
    #     plt.ylabel("# POSITIVE REWARDS")
    #     plt.plot(x, y, 'r')
    #     plt.savefig("score/progress.png")
    #     plt.close()


def preprocess_observation(obs, clip=200):
    return np.clip(obs, a_max=clip, a_min=-clip)




######################################################################
# Main training loop
# ------------------
#######################################################################
rewads_per_epoc = []
state_normalizer = Normalizer(STATE_DIM)
goal_normalizer = Normalizer(GOAL_DIM)



for i_epoc in range(MAX_EPOCS):

    # Will track each positive reward
    positive_rewards = 0
    for i_cycle in range(MAX_CYCLES):

        for i_episode in range(MAX_EPISODES):
            # Initialize the environment and state
            observation = env.reset()
            state = observation[STATE]

            state = state_normalizer.normalize(preprocess_observation(state))
            goal = observation[DESIRED_GOAL]
            goal = goal_normalizer.normalize(preprocess_observation(goal))
            state_goal = np.concatenate((state, goal))
            state_goal = torch.from_numpy(state_goal)
            state_action_pairs = []

            for t in range(MAX_TIME_STEPS):

                # Select and perform an action
                action = select_action(state_goal)
                # if torch.cuda.is_available():
                #     action = action.cuda()
                observation, reward, done, info = env.step(action.cpu().numpy())

                done = False
                if reward == 0:
                    positive_rewards += 1
                    done = True
                    if t == 0:
                        continue  # skip the state-goal pair where goal is already satisfied
                reward = torch.tensor([reward], device=device, dtype=torch.double)

                next_state = observation[STATE]
                achived_goal = observation[ACHIEVED_GOAL]
                next_state = state_normalizer.normalize(preprocess_observation(next_state))
                achived_goal = goal_normalizer.normalize(preprocess_observation(achived_goal))
                next_state_goal = np.concatenate((next_state, goal))
                next_state_goal = torch.from_numpy(next_state_goal)

                # Store transition in memory
                #replay_buffer.push(state_goal.cpu(), action.cpu(), next_state_goal.cpu(), reward.cpu(), info, t)
                state_action_pairs.append(State_Action(state_goal, action, next_state_goal, achived_goal, info, t))
                state_goal = next_state_goal

                if done:
                    print("Trovato #############################################")
                    break
                elif t == (MAX_TIME_STEPS - 1):
                    if dist.euclidean(goal, achived_goal) < 0.07:
                        positive_rewards += 1
                        reward = torch.tensor([0], dtype=torch.double)
                        print("Episodio finito reward 0 **********************************************")
                    else:
                        reward = torch.tensor([-1], device=device, dtype=torch.double)
                        print("Episodio finito reward -1")

            last_episode_duration = t
            for t in range(last_episode_duration):
                time_step_transition = state_action_pairs[t].time_step
                goals = future(time_step_transition, state_action_pairs, NUM_GOALS)
                goals.append(goal) #add the current goal to the replay
                for goal in goals:
                    reward = env.compute_reward(state_action_pairs[t].achived_goal, goal, state_action_pairs[t].info)
                    reward = torch.tensor([reward], dtype=torch.double)
                    state_goal = state_action_pairs[t].state
                    action = state_action_pairs[t].action
                    next_state_goal = state_action_pairs[t].next_state
                    # Store future transition in memory
                    replay_buffer.push(state_goal.cpu(), action.cpu(), next_state_goal.cpu(), reward.cpu(),
                                       state_action_pairs[t].info,
                                       time_step_transition)
            # End time step loop

            # Save the environment rendering
            # if RENDER_FLAG:
            #     with open('score/FetchPush-v1__EPOC_' + str(i_epoc) + '__CYCLE_' + str(i_cycle) + '.gif', 'wb') as f:
            #         im = Image.new('RGB', frames[0].size)
            #         im.save(f, save_all=True, append_images=frames)
            #         RENDER_FLAG = False
            #         frames = []

        # End episode loop
        time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        print("Starting optimizing at {}".format(time))
        for i_optim_step in range(MAX_OPTIMIZATION_STEPS):
            optimize_model()
        time = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        print("Optimization done at {}\tEpoc: {}\tCycle: {}".format(time, i_epoc, i_cycle))
        print("Optimization done at {}\tEpoc: {}\tCycle: {}".format(time, i_epoc, i_cycle),
              file=open('score/progress.txt', 'a'))
        # End optim loop
        Q, Q_target = updateTarget(Q, Q_target)
        mu, mu_target = updateTarget(mu, mu_target)
    # End cycle loop
    logg(mu, "logg_parameters/mu_parameters_" + str(i_epoc) + ".txt")
    logg(Q, "logg_parameters/Q_parameters_" + str(i_epoc) + ".txt")

    rewads_per_epoc.append(positive_rewards)
    plot_progress(100 * np.array(rewads_per_epoc) / (MAX_CYCLES * MAX_EPISODES))
    # plot_progrss_q_loss(loss_critic_list)
    torch.save(mu.state_dict(), 'model_trained/mu')
    torch.save(mu_target.state_dict(), 'model_trained/mu_target')
    torch.save(Q.state_dict(), 'model_trained/Q')
    torch.save(Q_target.state_dict(), 'model_trained/Q_target')
# End epoc loop
torch.save(mu.state_dict(), 'model_trained/mu_trained')
torch.save(mu_target.state_dict(), 'model_trained/mu_target_trained')
torch.save(Q.state_dict(), 'model_trained/Q_trained')
torch.save(Q_target.state_dict(), 'model_trained/Q_target_trained')
